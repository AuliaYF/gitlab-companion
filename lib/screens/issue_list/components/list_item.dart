import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gitlab_companion/blocs/issue_container/issue_container_bloc.dart';
import 'package:gitlab_companion/blocs/time_tracker/time_tracker_bloc.dart';
import 'package:gitlab_companion/components/index.dart';
import 'package:gitlab_companion/helpers/index.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class IssueListItem extends StatefulWidget {
  const IssueListItem({
    Key key,
    this.onPressed,
    @required this.issue,
  })  : assert(issue != null),
        super(key: key);

  final Function(models.Issue) onPressed;
  final models.Issue issue;

  @override
  _IssueListItemState createState() => _IssueListItemState();
}

class _IssueListItemState extends State<IssueListItem> {
  void _registerIssue() {
    final bloc = BlocProvider.of<IssueContainerBloc>(context);

    bloc.add(IssueContainerRegistered(issue: widget.issue));
  }

  @override
  void initState() {
    super.initState();

    _registerIssue();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<IssueContainerBloc, IssueContainerState>(
      condition: (prevState, nextState) {
        return !(nextState is IssueContainerInProgress);
      },
      builder: (context, state) {
        if (state is IssueContainerSuccess) {
          if (state.issues.containsKey(widget.issue.references.full)) {
            var issue = state.issues[widget.issue.references.full];
            return _MainView(
              onPressed: widget.onPressed == null
                  ? null
                  : () {
                      widget.onPressed(issue);
                    },
              issue: issue,
            );
          }
        }

        return _MainView(
          onPressed: widget.onPressed == null
              ? null
              : () {
                  widget.onPressed(widget.issue);
                },
          issue: widget.issue,
        );
      },
    );
  }
}

class _MainView extends StatelessWidget {
  const _MainView({
    Key key,
    this.onPressed,
    @required this.issue,
  })  : assert(issue != null),
        super(key: key);

  final Function onPressed;
  final models.Issue issue;

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: issue.references.full,
      child: Card(
        margin: EdgeInsets.zero,
        child: Material(
          color: Colors.transparent,
          clipBehavior: Clip.hardEdge,
          borderRadius: BorderRadius.circular(4.0),
          child: InkWell(
            onTap: onPressed,
            child: Container(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    issue.title,
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  if (issue.labels.isNotEmpty) ...[
                    SizedBox(height: 16.0),
                    LabelsBox(labels: issue.labels)
                  ],
                  SizedBox(height: 16.0),
                  BlocBuilder<TimeTrackerBloc, TimeTrackerState>(
                    condition: (prevState, nextState) {
                      return !(nextState is TimeTrackerInProgress);
                    },
                    builder: (context, state) {
                      if (state is TimeTrackerSuccess) {
                        if (state.timeStats
                            .containsKey(issue.references.full)) {
                          return TimeStatsBox(
                            timeStats:
                                state.timeStats[issue.references.full].copyWith(
                              timeEstimate: issue.timeStats.timeEstimate,
                              humanTimeEstimate:
                                  issue.timeStats.humanTimeEstimate,
                            ),
                          );
                        }
                      }

                      return TimeStatsBox(timeStats: issue.timeStats);
                    },
                  ),
                  if (issue.milestone != null || issue.dueDate != null) ...[
                    SizedBox(height: 16.0),
                    Row(
                      children: [
                        if (issue.milestone != null) ...[
                          Icon(
                            MdiIcons.clockOutline,
                            size:
                                Theme.of(context).textTheme.bodyText2.fontSize,
                            color: Theme.of(context).textTheme.bodyText2.color,
                          ),
                          SizedBox(width: 4.0),
                          Text(
                            issue.milestone.title,
                            style: Theme.of(context).textTheme.bodyText2,
                          ),
                        ],
                        if (issue.milestone != null && issue.dueDate != null)
                          SizedBox(width: 16.0),
                        if (issue.dueDate != null) ...[
                          Icon(
                            MdiIcons.calendarAlert,
                            size:
                                Theme.of(context).textTheme.bodyText2.fontSize,
                            color: DateHelper.isBeforeToday(issue.dueDate) &&
                                    issue.state == models.IssueState.opened
                                ? Colors.red
                                : Theme.of(context).textTheme.bodyText2.color,
                          ),
                          SizedBox(width: 4.0),
                          Text(
                            DateHelper.format(
                              issue.dueDate,
                              'MMM d, yyyy',
                            ),
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .copyWith(
                                  color:
                                      DateHelper.isBeforeToday(issue.dueDate) &&
                                              issue.state ==
                                                  models.IssueState.opened
                                          ? Colors.red
                                          : Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              .color,
                                ),
                          ),
                        ]
                      ],
                    ),
                  ],
                  SizedBox(height: 16.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        issue.references.full,
                        style: Theme.of(context).textTheme.caption,
                      ),
                      if ((issue?.assignee?.avatarUrl ?? '').isNotEmpty)
                        SizedBox(
                          width: 24.0,
                          height: 24.0,
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(
                              issue?.assignee?.avatarUrl,
                            ),
                          ),
                        )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
