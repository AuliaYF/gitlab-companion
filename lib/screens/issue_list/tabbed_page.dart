import 'package:flutter/material.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/screens/issue_list/page.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class TabbedIssueListPage extends StatefulWidget {
  @override
  _TabbedIssueListPageState createState() => _TabbedIssueListPageState();
}

class _TabbedIssueListPageState extends State<TabbedIssueListPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);

    return DefaultTabController(
      length: 3,
      child: NestedScrollView(
        headerSliverBuilder: (context, _) {
          return [
            SliverAppBar(
              floating: true,
              pinned: true,
              leading: const Icon(MdiIcons.gitlab),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('GitLab Companion'),
                  Text(
                    'Issues',
                    style: Theme.of(context).textTheme.caption,
                  ),
                ],
              ),
              bottom: TabBar(
                tabs: [
                  Tab(text: 'Open'),
                  Tab(text: 'Closed'),
                  Tab(text: 'All'),
                ],
              ),
            ),
          ];
        },
        body: TabBarView(
          children: [
            IssueListPage(state: models.IssueState.opened),
            IssueListPage(state: models.IssueState.closed),
            IssueListPage(),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
