import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gitlab_companion/components/index.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/screens/issue_detail/tabbed_page.dart';
import 'package:gitlab_companion/screens/issue_list/bloc/issue_list_bloc.dart';
import 'package:gitlab_companion/screens/issue_list/components/index.dart';
import 'package:gitlab_companion/services/index.dart';
import 'package:provider/provider.dart';

part 'screen.dart';

class IssueListPage extends StatefulWidget {
  final models.IssueState state;

  const IssueListPage({Key key, this.state}) : super(key: key);

  @override
  _IssueListPageState createState() => _IssueListPageState();
}

class _IssueListPageState extends State<IssueListPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    final dio = Provider.of<Dio>(context);
    final service = IssueService(dio);

    return BlocProvider<IssueListBloc>(
      create: (context) => IssueListBloc(
        service,
        state: widget.state,
      ),
      child: IssueListScreen(),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
