part of 'page.dart';

class IssueListScreen extends StatefulWidget {
  @override
  _IssueListScreenState createState() => _IssueListScreenState();
}

class _IssueListScreenState extends State<IssueListScreen> {
  Completer _completer = Completer();
  bool _hasMore = false;
  int _nextPage = 1;

  void _load({bool refresh = false}) {
    final bloc = BlocProvider.of<IssueListBloc>(context);

    bloc.add(IssueListLoaded(refresh: refresh, page: _nextPage));
  }

  Future<void> _onRefresh() {
    _nextPage = 1;
    _load(refresh: true);

    return _completer.future;
  }

  void _onIssuePressed(issue) {
    Navigator.push(
      context,
      MaterialPageRoute<void>(
        builder: (context) {
          return TabbedIssueDetailPage(
            issue: issue,
          );
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    _load(refresh: true);
  }

  @override
  void dispose() {
    _completer?.complete();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<IssueListBloc>(context);

    return BlocListener<IssueListBloc, IssueListState>(
      listener: (context, state) {
        if (state is IssueListSuccess && state.hasError) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(state.errorMessage),
              backgroundColor: Colors.red,
            ),
          );
        }

        if (state is IssueListSuccess && !state.hasError) {
          _hasMore = state.hasMore;
          _nextPage = state.nextPage;
        }

        if (state is IssueListSuccess || state is IssueListFailure) {
          _completer?.complete();

          _completer = Completer();
        }
      },
      child: RefreshIndicator(
        onRefresh: _onRefresh,
        child: ReachedBottomListener(
          onReached: () {
            if (!(bloc.state is IssueListInProgress) && _hasMore) {
              _load();
            }
          },
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(),
              BlocBuilder<IssueListBloc, IssueListState>(
                condition: (prevState, nextState) {
                  return !(nextState is IssueListInProgress);
                },
                builder: (context, state) {
                  if (state is IssueListFailure) {
                    return SliverFillRemaining(
                      child: ErrorBox(
                        errorMessage: state.errorMessage,
                        onRetry: () {
                          _nextPage = 1;
                          _load(refresh: true);
                        },
                      ),
                    );
                  } else if (state is IssueListSuccess) {
                    if (state.issues.isNotEmpty) {
                      return SliverPadding(
                        padding: const EdgeInsets.all(16.0),
                        sliver: SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (context, index) {
                              if (index == state.issues.length) {
                                return Padding(
                                  padding: const EdgeInsets.all(16.0).copyWith(
                                    bottom: 0.0,
                                  ),
                                  child: Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                );
                              }

                              var issue = state.issues.elementAt(index);

                              return Column(
                                children: [
                                  IssueListItem(
                                    onPressed: _onIssuePressed,
                                    issue: issue,
                                  ),
                                  if (index < state.issues.length - 1)
                                    SizedBox(height: 16.0),
                                ],
                              );
                            },
                            childCount:
                                state.issues.length + (state.hasMore ? 1 : 0),
                          ),
                        ),
                      );
                    } else {
                      return SliverFillRemaining(
                        child: InfoBox(
                          message: 'No issues available',
                        ),
                      );
                    }
                  }
                  return SliverFillRemaining(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
