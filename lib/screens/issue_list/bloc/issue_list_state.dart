part of 'issue_list_bloc.dart';

abstract class IssueListState extends Equatable {
  const IssueListState();
}

class IssueListInitial extends IssueListState {
  @override
  List<Object> get props => [];
}

class IssueListInProgress extends IssueListState {
  @override
  List<Object> get props => [];
}

class IssueListFailure extends IssueListState {
  final String errorMessage;

  IssueListFailure({this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}

class IssueListSuccess extends IssueListState {
  final bool hasError;
  final String errorMessage;
  final bool hasMore;
  final int nextPage;
  final List<models.Issue> issues;

  IssueListSuccess({
    this.hasError = false,
    this.errorMessage,
    this.hasMore = false,
    this.nextPage,
    this.issues,
  });

  @override
  List<Object> get props => [hasError, errorMessage, hasMore, nextPage, issues];
}
