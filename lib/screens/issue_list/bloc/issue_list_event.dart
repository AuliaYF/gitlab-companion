part of 'issue_list_bloc.dart';

abstract class IssueListEvent extends Equatable {
  const IssueListEvent();
}

class IssueListLoaded extends IssueListEvent {
  final bool refresh;
  final int page;

  IssueListLoaded({this.refresh = false, this.page = 1});

  @override
  List<Object> get props => [refresh, page];
}
