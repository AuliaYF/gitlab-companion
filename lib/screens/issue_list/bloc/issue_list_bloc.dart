import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/services/index.dart';
import 'package:rxdart/rxdart.dart';

part 'issue_list_event.dart';
part 'issue_list_state.dart';

class IssueListBloc extends Bloc<IssueListEvent, IssueListState> {
  final IssueService _service;
  final models.IssueState _state;

  IssueListBloc(this._service, {models.IssueState state}) : _state = state;

  @override
  IssueListState get initialState => IssueListInitial();

  @override
  Stream<Transition<IssueListEvent, IssueListState>> transformEvents(
      Stream<IssueListEvent> events, transitionFn) {
    return super.transformEvents(
      events.debounceTime(Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<IssueListState> mapEventToState(
    IssueListEvent event,
  ) async* {
    if (event is IssueListLoaded) {
      if (event.refresh) {
        yield IssueListInitial();
      }

      var issues = <models.Issue>[];

      if (state is IssueListSuccess) {
        issues.addAll((state as IssueListSuccess).issues);
      }

      yield IssueListInProgress();

      try {
        final response = await _service.fetchIssues(
          page: event.page,
          state: _state,
        );

        issues.addAll(response.data);

        yield IssueListSuccess(
          hasMore: response.hasMore,
          nextPage: response.nextPage,
          issues: issues,
        );
      } catch (e) {
        if (event.refresh) {
          yield IssueListFailure(errorMessage: e.toString());
        } else {
          yield IssueListSuccess(
            hasError: true,
            errorMessage: e.toString(),
            issues: issues,
          );
        }
      }
    }
  }
}
