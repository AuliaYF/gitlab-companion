import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gitlab_companion/components/index.dart';
import 'package:gitlab_companion/screens/home/page.dart';
import 'package:gitlab_companion/screens/setup/page.dart';
import 'package:gitlab_companion/screens/splash/bloc/splash_bloc.dart';
import 'package:gitlab_companion/services/index.dart';
import 'package:gitlab_companion/session.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

part 'screen.dart';

class SplashPage extends StatelessWidget {
  static final String routeName = '/';

  @override
  Widget build(BuildContext context) {
    final dio = Provider.of<Dio>(context);
    final service = UserService(dio);

    return BlocProvider<SplashBloc>(
      create: (context) => SplashBloc(service),
      child: Scaffold(
        body: SplashScreen(),
      ),
    );
  }
}
