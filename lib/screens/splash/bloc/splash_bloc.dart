import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gitlab_companion/helpers/index.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/services/index.dart';

part 'splash_event.dart';
part 'splash_state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final UserService _service;

  SplashBloc(this._service);

  @override
  SplashState get initialState => SplashInitial();

  @override
  Stream<SplashState> mapEventToState(
    SplashEvent event,
  ) async* {
    if (event is SplashStarted) {
      yield SplashInitial();

      await Future.delayed(Duration(seconds: 1));

      var hasPersonalToken = await PreferenceHelper.hasPersonalToken();

      if (hasPersonalToken) {
        try {
          final user = await _service.fetchCurrentUser();

          yield SplashLaunchHome(currentUser: user);
        } catch (e) {
          yield SplashFailure(errorMessage: e.toString());
        }
      } else {
        yield SplashLaunchSetup();
      }
    }
  }
}
