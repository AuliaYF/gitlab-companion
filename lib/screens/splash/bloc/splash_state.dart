part of 'splash_bloc.dart';

abstract class SplashState extends Equatable {
  const SplashState();
}

class SplashInitial extends SplashState {
  @override
  List<Object> get props => [];
}

class SplashFailure extends SplashState {
  final String errorMessage;

  SplashFailure({this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}

class SplashLaunchSetup extends SplashState {
  @override
  List<Object> get props => [];
}

class SplashLaunchHome extends SplashState {
  final models.User currentUser;

  SplashLaunchHome({this.currentUser});

  @override
  List<Object> get props => [currentUser];
}
