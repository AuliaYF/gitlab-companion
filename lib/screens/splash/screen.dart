part of 'page.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void _load() {
    final bloc = BlocProvider.of<SplashBloc>(context);
    bloc.add(SplashStarted());
  }

  @override
  void initState() {
    super.initState();

    _load();
  }

  @override
  Widget build(BuildContext context) {
    final session = Provider.of<Session>(context);

    return BlocListener<SplashBloc, SplashState>(
      listener: (context, state) {
        if (state is SplashLaunchHome) {
          session.currentUser = state.currentUser;

          Navigator.pushReplacementNamed(context, HomePage.routeName);
        } else if (state is SplashLaunchSetup) {
          Navigator.pushReplacementNamed(context, SetupPage.routeName);
        }
      },
      child: Container(
        padding: const EdgeInsets.all(32.0),
        width: double.infinity,
        child: Column(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    MdiIcons.gitlab,
                    size: 96.0,
                  ),
                  SizedBox(height: 32.0),
                  RichText(
                    text: TextSpan(
                      style: Theme.of(context).textTheme.headline6,
                      children: [
                        TextSpan(
                          text: 'Another ',
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                        TextSpan(
                          text: 'GitLab',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: ' Companion',
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            BlocBuilder<SplashBloc, SplashState>(
              builder: (context, state) {
                if (state is SplashFailure) {
                  return ErrorBox(
                    showIcon: false,
                    errorMessage: state.errorMessage,
                    onRetry: _load,
                  );
                }

                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
