part of 'page.dart';

class SetupScreen extends StatefulWidget {
  @override
  _SetupScreenState createState() => _SetupScreenState();
}

class _SetupScreenState extends State<SetupScreen> {
  void _onSubmit() {
    var formState = FormBuilder.of(context);
    var bloc = BlocProvider.of<SetupBloc>(context);

    FocusScope.of(context).requestFocus(FocusNode());

    if (bloc.state is SetupInProgress) return;

    if (formState.saveAndValidate()) {
      bloc.add(
        SetupStarted(personalToken: formState.value['personalToken']),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final session = Provider.of<Session>(context);

    return BlocListener<SetupBloc, SetupState>(
      listener: (context, state) {
        if (state is SetupFailure) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(state.errorMessage),
              backgroundColor: Colors.red,
            ),
          );
        } else if (state is SetupSuccess) {
          session.currentUser = state.currentUser;

          Navigator.pushReplacementNamed(context, HomePage.routeName);
        }
      },
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(32.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: const Icon(
                MdiIcons.gitlab,
                size: 96.0,
              ),
            ),
            FormBuilderTextField(
              attribute: 'personalToken',
              maxLines: 1,
              decoration: InputDecoration(
                filled: true,
                labelText: 'Personal Token',
              ),
              validators: [
                FormBuilderValidators.required(),
              ],
            ),
            SizedBox(height: 12.0),
            RaisedButton(
              onPressed: _onSubmit,
              child: BlocBuilder<SetupBloc, SetupState>(
                builder: (context, state) {
                  if (state is SetupInProgress) {
                    return SizedBox(
                      width: 16.0,
                      height: 16.0,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }

                  return const Text('SETUP');
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
