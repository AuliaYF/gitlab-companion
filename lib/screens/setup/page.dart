import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:gitlab_companion/screens/home/page.dart';
import 'package:gitlab_companion/services/index.dart';
import 'package:gitlab_companion/session.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

import 'bloc/setup_bloc.dart';

part 'screen.dart';

class SetupPage extends StatelessWidget {
  static final String routeName = '/setup';

  @override
  Widget build(BuildContext context) {
    final dio = Provider.of<Dio>(context);
    final service = UserService(dio);

    return BlocProvider<SetupBloc>(
      create: (context) => SetupBloc(service),
      child: FormBuilder(
        child: Scaffold(
          body: SetupScreen(),
        ),
      ),
    );
  }
}
