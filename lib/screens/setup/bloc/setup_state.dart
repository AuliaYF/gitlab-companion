part of 'setup_bloc.dart';

abstract class SetupState extends Equatable {
  const SetupState();
}

class SetupInitial extends SetupState {
  @override
  List<Object> get props => [];
}

class SetupInProgress extends SetupState {
  @override
  List<Object> get props => throw UnimplementedError();
}

class SetupFailure extends SetupState {
  final String errorMessage;

  SetupFailure({this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}

class SetupSuccess extends SetupState {
  final models.User currentUser;

  SetupSuccess({this.currentUser});

  @override
  List<Object> get props => [currentUser];
}
