import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gitlab_companion/helpers/index.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/services/index.dart';

part 'setup_event.dart';
part 'setup_state.dart';

class SetupBloc extends Bloc<SetupEvent, SetupState> {
  final UserService _service;

  SetupBloc(this._service);

  @override
  SetupState get initialState => SetupInitial();

  @override
  Stream<SetupState> mapEventToState(
    SetupEvent event,
  ) async* {
    if (event is SetupStarted) {
      yield SetupInProgress();

      await Future.delayed(Duration(seconds: 1));

      try {
        await PreferenceHelper.setPersonalToken(event.personalToken);
        final user = await _service.fetchCurrentUser();

        yield SetupSuccess(currentUser: user);
      } catch (e) {
        yield SetupFailure(errorMessage: e.toString());
      }
    }
  }
}
