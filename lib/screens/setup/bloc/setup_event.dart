part of 'setup_bloc.dart';

abstract class SetupEvent extends Equatable {
  const SetupEvent();
}

class SetupStarted extends SetupEvent {
  final String personalToken;

  SetupStarted({this.personalToken});

  @override
  List<Object> get props => [personalToken];
}
