import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gitlab_companion/components/index.dart';
import 'package:gitlab_companion/screens/project_list/bloc/project_list_bloc.dart';
import 'package:gitlab_companion/screens/project_list/components/index.dart';
import 'package:gitlab_companion/services/index.dart';
import 'package:gitlab_companion/session.dart';
import 'package:provider/provider.dart';

part 'screen.dart';

class ProjectListPage extends StatefulWidget {
  final bool personal;

  const ProjectListPage({Key key, this.personal = false}) : super(key: key);

  @override
  _ProjectListPageState createState() => _ProjectListPageState();
}

class _ProjectListPageState extends State<ProjectListPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);

    final session = Provider.of<Session>(context);
    final dio = Provider.of<Dio>(context);
    final service = ProjectService(dio);

    return BlocProvider<ProjectListBloc>(
      create: (context) => ProjectListBloc(
        service,
        personal: widget.personal,
        userId: session.currentUser.id,
      ),
      child: ProjectListScreen(),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
