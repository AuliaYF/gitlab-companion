import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ProjectListItem extends StatelessWidget {
  const ProjectListItem({
    Key key,
    @required this.title,
    this.stars = 0,
    this.forks = 0,
    this.issues = 0,
  }) : super(key: key);

  final String title;
  final int stars;
  final int forks;
  final int issues;

  @override
  Widget build(BuildContext context) {
    var titles = title.split(' / ');
    var titleWidgets = titles.map((part) {
      return TextSpan(
        text: part != titles.last ? '${part} / ' : part,
        style: TextStyle(
          fontWeight: part != titles.last ? FontWeight.normal : FontWeight.bold,
        ),
      );
    }).toList();
    var initials = titles.last
        .split(' ')
        .map((part) {
          return part.trim().isEmpty ? '' : part.substring(0, 1);
        })
        .where((initial) => initial.isNotEmpty)
        .take(3)
        .join('')
        .toUpperCase();

    return Card(
      margin: EdgeInsets.zero,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          children: [
            Container(
              width: 48.0,
              height: 48.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                color: Colors.white,
              ),
              child: Center(
                child: Text(
                  initials,
                  style: Theme.of(context)
                      .textTheme
                      .headline6
                      .copyWith(color: Colors.black),
                ),
              ),
            ),
            SizedBox(width: 16.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  RichText(
                    text: TextSpan(children: titleWidgets),
                  ),
                  SizedBox(height: 16.0),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        MdiIcons.star,
                        size: Theme.of(context).textTheme.bodyText2.fontSize,
                        color: Theme.of(context).textTheme.bodyText2.color,
                      ),
                      SizedBox(width: 4.0),
                      Text(
                        stars.toString(),
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                      SizedBox(width: 16.0),
                      Icon(
                        MdiIcons.sourceFork,
                        size: Theme.of(context).textTheme.bodyText2.fontSize,
                        color: Theme.of(context).textTheme.bodyText2.color,
                      ),
                      SizedBox(width: 4.0),
                      Text(
                        forks.toString(),
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                      SizedBox(width: 16.0),
                      Icon(
                        MdiIcons.cards,
                        size: Theme.of(context).textTheme.bodyText2.fontSize,
                        color: Theme.of(context).textTheme.bodyText2.color,
                      ),
                      SizedBox(width: 4.0),
                      Text(
                        issues.toString(),
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
