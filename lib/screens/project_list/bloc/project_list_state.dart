part of 'project_list_bloc.dart';

abstract class ProjectListState extends Equatable {
  const ProjectListState();
}

class ProjectListInitial extends ProjectListState {
  @override
  List<Object> get props => [];
}

class ProjectListInProgress extends ProjectListState {
  @override
  List<Object> get props => [];
}

class ProjectListFailure extends ProjectListState {
  final String errorMessage;

  ProjectListFailure({this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}

class ProjectListSuccess extends ProjectListState {
  final bool hasError;
  final String errorMessage;
  final bool hasMore;
  final int nextPage;
  final List<models.Project> projects;

  ProjectListSuccess({
    this.hasError = false,
    this.errorMessage,
    this.hasMore = false,
    this.nextPage,
    this.projects,
  });

  @override
  List<Object> get props => [
        hasError,
        errorMessage,
        errorMessage,
        hasMore,
        nextPage,
        projects,
      ];
}
