part of 'project_list_bloc.dart';

abstract class ProjectListEvent extends Equatable {
  const ProjectListEvent();
}

class ProjectListLoaded extends ProjectListEvent {
  final bool refresh;
  final int page;

  ProjectListLoaded({this.refresh = false, this.page = 1});

  @override
  List<Object> get props => [refresh, page];
}
