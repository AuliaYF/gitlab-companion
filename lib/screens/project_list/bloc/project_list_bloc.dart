import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/responses/index.dart';
import 'package:gitlab_companion/services/index.dart';
import 'package:rxdart/rxdart.dart';

part 'project_list_event.dart';
part 'project_list_state.dart';

class ProjectListBloc extends Bloc<ProjectListEvent, ProjectListState> {
  final ProjectService _service;
  final bool _personal;
  final int _userId;

  ProjectListBloc(this._service, {bool personal = false, int userId})
      : _personal = personal,
        _userId = userId;

  @override
  ProjectListState get initialState => ProjectListInitial();

  @override
  Stream<Transition<ProjectListEvent, ProjectListState>> transformEvents(
      Stream<ProjectListEvent> events, transitionFn) {
    return super.transformEvents(
      events.debounceTime(Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<ProjectListState> mapEventToState(
    ProjectListEvent event,
  ) async* {
    if (event is ProjectListLoaded) {
      if (event.refresh) {
        yield ProjectListInitial();
      }

      var projects = <models.Project>[];

      if (state is ProjectListSuccess) {
        projects.addAll((state as ProjectListSuccess).projects);
      }

      yield ProjectListInProgress();

      try {
        FetchListResponse<models.Project> response;

        if (_personal) {
          response = await _service.fetchPersonalProjects(
            _userId,
            page: event.page,
          );
        } else {
          response = await _service.fetchProjects(page: event.page);
        }

        projects.addAll(response.data);

        yield ProjectListSuccess(
          hasMore: response.hasMore,
          nextPage: response.nextPage,
          projects: projects,
        );
      } catch (e) {
        if (event.refresh) {
          yield ProjectListFailure(errorMessage: e.toString());
        } else {
          yield ProjectListSuccess(
            hasError: true,
            errorMessage: e.toString(),
            projects: projects,
          );
        }
      }
    }
  }
}
