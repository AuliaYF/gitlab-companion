import 'package:flutter/material.dart';
import 'package:gitlab_companion/screens/project_list/page.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class TabbedProjectListPage extends StatefulWidget {
  @override
  _TabbedProjectListPageState createState() => _TabbedProjectListPageState();
}

class _TabbedProjectListPageState extends State<TabbedProjectListPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);

    return DefaultTabController(
      length: 2,
      child: NestedScrollView(
        headerSliverBuilder: (context, _) {
          return [
            SliverAppBar(
              floating: true,
              pinned: true,
              leading: const Icon(MdiIcons.gitlab),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('GitLab Companion'),
                  Text(
                    'Projects',
                    style: Theme.of(context).textTheme.caption,
                  ),
                ],
              ),
              bottom: TabBar(
                tabs: [
                  Tab(text: 'All'),
                  Tab(text: 'Personal'),
                ],
              ),
            ),
          ];
        },
        body: TabBarView(
          children: [
            ProjectListPage(),
            ProjectListPage(personal: true),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
