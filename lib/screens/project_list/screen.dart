part of 'page.dart';

class ProjectListScreen extends StatefulWidget {
  @override
  _ProjectListScreenState createState() => _ProjectListScreenState();
}

class _ProjectListScreenState extends State<ProjectListScreen> {
  Completer _completer = Completer();
  bool _hasMore = false;
  int _nextPage = 1;

  void _load({bool refresh = false}) {
    final bloc = BlocProvider.of<ProjectListBloc>(context);

    bloc.add(ProjectListLoaded(refresh: refresh, page: _nextPage));
  }

  Future<void> _onRefresh() {
    _nextPage = 1;
    _load(refresh: true);

    return _completer.future;
  }

  @override
  void initState() {
    super.initState();

    _load(refresh: true);
  }

  @override
  void dispose() {
    _completer?.complete();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProjectListBloc>(context);

    return BlocListener<ProjectListBloc, ProjectListState>(
      listener: (context, state) {
        if (state is ProjectListSuccess && state.hasError) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(state.errorMessage),
              backgroundColor: Colors.red,
            ),
          );
        }

        if (state is ProjectListSuccess && !state.hasError) {
          _hasMore = state.hasMore;
          _nextPage = state.nextPage;
        }

        if (state is ProjectListSuccess || state is ProjectListFailure) {
          _completer?.complete();

          _completer = Completer();
        }
      },
      child: RefreshIndicator(
        onRefresh: _onRefresh,
        child: ReachedBottomListener(
          onReached: () {
            if (!(bloc.state is ProjectListInProgress) && _hasMore) {
              _load();
            }
          },
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(),
              BlocBuilder<ProjectListBloc, ProjectListState>(
                condition: (prevState, nextState) {
                  return !(nextState is ProjectListInProgress);
                },
                builder: (context, state) {
                  if (state is ProjectListFailure) {
                    return SliverFillRemaining(
                      child: ErrorBox(
                        errorMessage: state.errorMessage,
                        onRetry: () {
                          _nextPage = 1;
                          _load(refresh: true);
                        },
                      ),
                    );
                  } else if (state is ProjectListSuccess) {
                    if (state.projects.isNotEmpty) {
                      return SliverPadding(
                        padding: const EdgeInsets.all(16.0),
                        sliver: SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (context, index) {
                              if (index == state.projects.length) {
                                return Padding(
                                  padding: const EdgeInsets.all(16.0).copyWith(
                                    bottom: 0.0,
                                  ),
                                  child: Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                );
                              }

                              var project = state.projects.elementAt(index);

                              return Column(
                                children: [
                                  ProjectListItem(
                                    title: project.nameWithNamespace,
                                    stars: project.totalStars,
                                    forks: project.totalForks,
                                    issues: project.totalIssues,
                                  ),
                                  if (index < state.projects.length - 1)
                                    SizedBox(height: 16.0),
                                ],
                              );
                            },
                            childCount:
                                state.projects.length + (state.hasMore ? 1 : 0),
                          ),
                        ),
                      );
                    } else {
                      return SliverFillRemaining(
                        child: InfoBox(
                          message: 'No projects available',
                        ),
                      );
                    }
                  }

                  return SliverFillRemaining(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
