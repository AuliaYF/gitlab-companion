part of 'activity_page.dart';

class IssueActivityScreen extends StatefulWidget {
  const IssueActivityScreen({
    Key key,
    @required this.issue,
  }) : super(key: key);

  final models.Issue issue;

  @override
  _IssueActivityScreenState createState() => _IssueActivityScreenState();
}

class _IssueActivityScreenState extends State<IssueActivityScreen> {
  Completer _completer = Completer();
  bool _hasMore = false;
  int _nextPage = 1;

  void _load({bool refresh = false}) {
    final bloc = BlocProvider.of<IssueNotesBloc>(context);

    bloc.add(IssueNotesLoaded(refresh: refresh, page: _nextPage));
  }

  Future<void> _onRefresh() {
    _nextPage = 1;
    _load(refresh: true);

    return _completer.future;
  }

  @override
  void initState() {
    super.initState();

    _load(refresh: true);
  }

  @override
  void dispose() {
    _completer?.complete();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final session = Provider.of<Session>(context);
    final bloc = BlocProvider.of<IssueNotesBloc>(context);

    return BlocListener<IssueNotesBloc, IssueNotesState>(
      listener: (context, state) {
        if (state is IssueNotesSuccess && state.hasError) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(state.errorMessage),
              backgroundColor: Colors.red,
            ),
          );
        }

        if (state is IssueNotesSuccess && !state.hasError) {
          _hasMore = state.hasMore;
          _nextPage = state.nextPage;
        }

        if (state is IssueNotesSuccess || state is IssueNotesFailure) {
          _completer?.complete();

          _completer = Completer();
        }
      },
      child: RefreshIndicator(
        onRefresh: _onRefresh,
        child: ReachedBottomListener(
          onReached: () {
            if (!(bloc.state is IssueNotesInProgress) && _hasMore) {
              _load();
            }
          },
          child: CustomScrollView(
            slivers: [
              SliverPadding(
                padding: const EdgeInsets.all(16.0).copyWith(bottom: 0.0),
                sliver: SliverToBoxAdapter(
                  child: IssueListItem(issue: widget.issue),
                ),
              ),
              BlocBuilder<IssueNotesBloc, IssueNotesState>(
                condition: (prevState, nextState) {
                  return !(nextState is IssueNotesInProgress);
                },
                builder: (context, state) {
                  if (state is IssueNotesFailure) {
                    return SliverPadding(
                      padding: const EdgeInsets.all(16.0).copyWith(
                        bottom: 0.0,
                      ),
                      sliver: SliverToBoxAdapter(
                        child: ErrorBox(
                          errorMessage: state.errorMessage,
                          onRetry: _onRefresh,
                        ),
                      ),
                    );
                  } else if (state is IssueNotesSuccess) {
                    if (state.notes.isNotEmpty) {
                      return SliverPadding(
                        padding: const EdgeInsets.all(16.0).copyWith(
                          bottom: 0.0,
                        ),
                        sliver: SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (context, index) {
                              if (index == state.notes.length) {
                                return Padding(
                                  padding: const EdgeInsets.all(16.0).copyWith(
                                    bottom: 0.0,
                                  ),
                                  child: Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                );
                              }

                              var position = NotePosition.first;

                              if (index > 0 && index < state.notes.length - 1) {
                                position = NotePosition.middle;
                              } else if (index == state.notes.length - 1) {
                                position = NotePosition.last;
                              }

                              var note = state.notes.elementAt(index);

                              return NoteListItem(
                                noLine: state.notes.length == 1,
                                position: position,
                                author: note.author,
                                body: note.body,
                                dateTime: note.updatedAt,
                              );
                            },
                            childCount:
                                state.notes.length + (state.hasMore ? 1 : 0),
                          ),
                        ),
                      );
                    } else {
                      return SliverToBoxAdapter();
                    }
                  }

                  return SliverPadding(
                    padding: const EdgeInsets.all(16.0).copyWith(bottom: 0.0),
                    sliver: SliverToBoxAdapter(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  );
                },
              ),
              SliverPadding(
                padding: const EdgeInsets.all(16.0),
                sliver: SliverToBoxAdapter(
                  child: NoteFormBox(user: session.currentUser),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
