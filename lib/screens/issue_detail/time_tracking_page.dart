import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gitlab_companion/blocs/issue_container/issue_container_bloc.dart';
import 'package:gitlab_companion/blocs/time_tracker/time_tracker_bloc.dart';
import 'package:gitlab_companion/components/index.dart';
import 'package:gitlab_companion/models/index.dart' as models;

part 'time_tracking_screen.dart';

class IssueTimeTrackingPage extends StatefulWidget {
  final models.Issue issue;

  const IssueTimeTrackingPage({Key key, @required this.issue})
      : assert(issue != null),
        super(key: key);

  @override
  _IssueTimeTrackingPageState createState() => _IssueTimeTrackingPageState();
}

class _IssueTimeTrackingPageState extends State<IssueTimeTrackingPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);

    return IssueTimeTrackingScreen(issue: widget.issue);
  }

  @override
  bool get wantKeepAlive => true;
}
