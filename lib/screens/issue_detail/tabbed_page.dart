import 'package:flutter/material.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/screens/issue_detail/activity_page.dart';
import 'package:gitlab_companion/screens/issue_detail/time_tracking_page.dart';

class TabbedIssueDetailPage extends StatelessWidget {
  final models.Issue issue;

  const TabbedIssueDetailPage({Key key, @required this.issue})
      : assert(issue != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        body: NestedScrollView(
          headerSliverBuilder: (context, _) {
            return [
              SliverAppBar(
                floating: true,
                pinned: true,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Issue ${issue.references.short}'),
                    Text(
                      issue.references.full,
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ],
                ),
                actions: [
                  if (issue.state == models.IssueState.opened)
                    FlatButton(
                      onPressed: () {},
                      child: const Text('CLOSE'),
                    ),
                  if (issue.state == models.IssueState.closed)
                    FlatButton(
                      onPressed: () {},
                      child: const Text('REOPEN'),
                    ),
                ],
                bottom: TabBar(
                  tabs: [
                    Tab(text: 'ACTIVITY'),
                    Tab(text: 'TIME TRACKING'),
                  ],
                ),
              ),
            ];
          },
          body: TabBarView(
            children: [
              IssueActivityPage(issue: issue),
              IssueTimeTrackingPage(issue: issue),
            ],
          ),
        ),
      ),
    );
  }
}
