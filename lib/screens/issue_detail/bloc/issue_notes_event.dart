part of 'issue_notes_bloc.dart';

abstract class IssueNotesEvent extends Equatable {
  const IssueNotesEvent();
}

class IssueNotesLoaded extends IssueNotesEvent {
  final bool refresh;
  final int page;

  IssueNotesLoaded({this.refresh = false, this.page = 1});

  @override
  List<Object> get props => [refresh, page];
}
