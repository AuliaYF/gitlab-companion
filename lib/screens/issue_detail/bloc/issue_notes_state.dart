part of 'issue_notes_bloc.dart';

abstract class IssueNotesState extends Equatable {
  const IssueNotesState();
}

class IssueNotesInitial extends IssueNotesState {
  @override
  List<Object> get props => [];
}

class IssueNotesInProgress extends IssueNotesState {
  @override
  List<Object> get props => [];
}

class IssueNotesFailure extends IssueNotesState {
  final String errorMessage;

  IssueNotesFailure({this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}

class IssueNotesSuccess extends IssueNotesState {
  final bool hasError;
  final String errorMessage;
  final bool hasMore;
  final int nextPage;
  final List<models.Note> notes;

  IssueNotesSuccess({
    this.hasError = false,
    this.errorMessage,
    this.hasMore = false,
    this.nextPage,
    this.notes,
  });

  @override
  List<Object> get props => [hasError, errorMessage, hasMore, nextPage, notes];
}
