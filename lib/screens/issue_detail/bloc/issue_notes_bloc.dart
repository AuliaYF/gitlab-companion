import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/services/index.dart';
import 'package:rxdart/rxdart.dart';

part 'issue_notes_event.dart';
part 'issue_notes_state.dart';

class IssueNotesBloc extends Bloc<IssueNotesEvent, IssueNotesState> {
  final NoteService _service;
  final int _projectId;
  final int _iid;

  IssueNotesBloc(this._service, this._projectId, this._iid);

  @override
  Stream<Transition<IssueNotesEvent, IssueNotesState>> transformEvents(
      Stream<IssueNotesEvent> events, transitionFn) {
    return super.transformEvents(
      events.debounceTime(Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  IssueNotesState get initialState => IssueNotesInitial();

  @override
  Stream<IssueNotesState> mapEventToState(
    IssueNotesEvent event,
  ) async* {
    if (event is IssueNotesLoaded) {
      if (event.refresh) {
        yield IssueNotesInitial();
      }

      var notes = <models.Note>[];

      if (state is IssueNotesSuccess) {
        notes.addAll((state as IssueNotesSuccess).notes);
      }

      yield IssueNotesInProgress();

      try {
        final response = await _service.fetchIssueNotes(
          _projectId,
          _iid,
          page: event.page,
        );

        notes.addAll(response.data);

        yield IssueNotesSuccess(
          hasMore: response.hasMore,
          nextPage: response.nextPage,
          notes: notes,
        );
      } catch (e) {
        if (event.refresh) {
          yield IssueNotesFailure(errorMessage: e.toString());
        } else {
          yield IssueNotesSuccess(
            hasError: true,
            errorMessage: e.toString(),
            notes: notes,
          );
        }
      }
    }
  }
}
