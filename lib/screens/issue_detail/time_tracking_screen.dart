part of 'time_tracking_page.dart';

class IssueTimeTrackingScreen extends StatefulWidget {
  final models.Issue issue;

  const IssueTimeTrackingScreen({Key key, @required this.issue})
      : assert(issue != null),
        super(key: key);

  @override
  _IssueTimeTrackingScreenState createState() =>
      _IssueTimeTrackingScreenState();
}

class _IssueTimeTrackingScreenState extends State<IssueTimeTrackingScreen> {
  bool _syncing = false;

  void _onStarted(timeStats) {
    final bloc = BlocProvider.of<TimeTrackerBloc>(context);

    bloc.add(TimeTrackerObjectStarted(
      objectId: widget.issue.references.full,
      initialTimeStats: timeStats,
    ));
  }

  void _onStopped(timeStats) {
    final bloc = BlocProvider.of<TimeTrackerBloc>(context);

    bloc.add(TimeTrackerObjectStopped(
      objectId: widget.issue.references.full,
    ));
  }

  void _onSynced(timeStats) {
    final bloc = BlocProvider.of<TimeTrackerBloc>(context);

    bloc.add(TimeTrackerIssueSynced(
      projectId: widget.issue.projectId,
      iid: widget.issue.iid,
      objectId: widget.issue.references.full,
    ));
  }

  void _onRemoved(timeStats) {
    final bloc = BlocProvider.of<TimeTrackerBloc>(context);

    bloc.add(TimeTrackerObjectRemoved(
      objectId: widget.issue.references.full,
    ));
  }

  void _updateIssue() {
    final bloc = BlocProvider.of<IssueContainerBloc>(context);

    bloc.add(IssueContainerUpdated(
      projectId: widget.issue.projectId,
      iid: widget.issue.iid,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Center(
        child: Container(
          width: 240.0,
          height: 240.0,
          child: MultiBlocListener(
            listeners: [
              BlocListener<TimeTrackerBloc, TimeTrackerState>(
                listener: (context, state) {
                  if (state is TimeTrackerInProgress &&
                      state.syncingFromObjectId ==
                          widget.issue.references.full) {
                    setState(() {
                      _syncing = true;
                    });
                  } else if (state is TimeTrackerSuccess &&
                      state.syncingFromObjectId ==
                          widget.issue.references.full) {
                    setState(() {
                      _syncing = false;

                      _updateIssue();
                    });
                  }
                },
              ),
              BlocListener<IssueContainerBloc, IssueContainerState>(
                listener: (context, state) {
                  if (state is IssueContainerSuccess &&
                      state.syncFromObjectId == widget.issue.references.full) {
                    _onRemoved(null);
                  }
                },
              )
            ],
            child: BlocBuilder<TimeTrackerBloc, TimeTrackerState>(
              condition: (prevState, nextState) {
                return !(nextState is TimeTrackerInProgress);
              },
              builder: (context, state) {
                if (state is TimeTrackerSuccess) {
                  if (state.timeStats
                      .containsKey(widget.issue.references.full)) {
                    return TimeTrackerBox(
                      timeStats: state.timeStats[widget.issue.references.full],
                      syncing: _syncing,
                      onStarted: _onStarted,
                      onStopped: _onStopped,
                      onSynced: _onSynced,
                      onRemoved: _onRemoved,
                    );
                  }
                }

                return BlocBuilder<IssueContainerBloc, IssueContainerState>(
                  condition: (prevState, nextState) {
                    return !(nextState is IssueContainerInProgress);
                  },
                  builder: (context, state) {
                    if (state is IssueContainerSuccess) {
                      if (state.issues
                          .containsKey(widget.issue.references.full)) {
                        return TimeTrackerBox(
                          timeStats: state
                              .issues[widget.issue.references.full].timeStats,
                          onStarted: _onStarted,
                          onStopped: _onStopped,
                        );
                      }
                    }
                    return TimeTrackerBox(
                      timeStats: widget.issue.timeStats,
                      onStarted: _onStarted,
                      onStopped: _onStopped,
                    );
                  },
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
