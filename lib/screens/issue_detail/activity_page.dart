import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gitlab_companion/components/index.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/screens/issue_detail/bloc/issue_notes_bloc.dart';
import 'package:gitlab_companion/screens/issue_list/components/index.dart';
import 'package:gitlab_companion/services/index.dart';
import 'package:gitlab_companion/session.dart';
import 'package:provider/provider.dart';

part 'activity_screen.dart';

class IssueActivityPage extends StatefulWidget {
  final models.Issue issue;

  const IssueActivityPage({Key key, @required this.issue})
      : assert(issue != null),
        super(key: key);

  @override
  _IssueActivityPageState createState() => _IssueActivityPageState();
}

class _IssueActivityPageState extends State<IssueActivityPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);

    final dio = Provider.of<Dio>(context);
    final service = NoteService(dio);

    return BlocProvider<IssueNotesBloc>(
      create: (context) => IssueNotesBloc(
        service,
        widget.issue.projectId,
        widget.issue.iid,
      ),
      child: IssueActivityScreen(issue: widget.issue),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
