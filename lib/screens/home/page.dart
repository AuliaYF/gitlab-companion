import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gitlab_companion/blocs/time_tracker/time_tracker_bloc.dart';
import 'package:gitlab_companion/helpers/index.dart';
import 'package:gitlab_companion/screens/issue_list/tabbed_page.dart';
import 'package:gitlab_companion/screens/merge_request_list/tabbed_page.dart';
import 'package:gitlab_companion/screens/project_list/tabbed_page.dart';
import 'package:gitlab_companion/screens/splash/page.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class HomePage extends StatefulWidget {
  static final String routeName = '/home';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentPageIndex = 0;
  PageController _pageController;

  void _startTimeTracker() {
    final bloc = BlocProvider.of<TimeTrackerBloc>(context);

    bloc.add(TimeTrackerStarted());
  }

  @override
  void initState() {
    super.initState();

    _startTimeTracker();

    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bottomNavItems = [
      BottomNavyBarItem(
        inactiveColor: Colors.teal[300],
        activeColor: Colors.teal,
        textAlign: TextAlign.center,
        icon: const Icon(MdiIcons.folder),
        title: const Text(
          'Projects',
        ),
      ),
      BottomNavyBarItem(
        inactiveColor: Colors.green[300],
        activeColor: Colors.green,
        textAlign: TextAlign.center,
        icon: const Icon(MdiIcons.cards),
        title: const Text(
          'Issues',
        ),
      ),
      BottomNavyBarItem(
        inactiveColor: Colors.orange[300],
        activeColor: Colors.orange,
        textAlign: TextAlign.center,
        icon: const Icon(MdiIcons.merge),
        title: const Text(
          'Merge Requests',
        ),
      ),
      BottomNavyBarItem(
        inactiveColor: Colors.blue[300],
        activeColor: Colors.blue,
        textAlign: TextAlign.center,
        icon: const Icon(MdiIcons.logout),
        title: const Text('Logout'),
      )
    ];

    return Scaffold(
      body: SizedBox.expand(
        child: PageView(
          controller: _pageController,
          onPageChanged: (index) {
            setState(() {
              _currentPageIndex = index;
            });
          },
          children: [
            TabbedProjectListPage(),
            TabbedIssueListPage(),
            TabbedMergeRequestListPage(),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentPageIndex,
        onItemSelected: (index) async {
          if (index == bottomNavItems.indexOf(bottomNavItems.last)) {
            var result = await showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: const Text('Log out from GitLab Companion?'),
                      actions: [
                        FlatButton(
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                          child: const Text('CANCEL'),
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.pop(context, true);
                          },
                          child: const Text('LOGOUT'),
                        ),
                      ],
                    );
                  },
                ) ??
                false;

            if (result) {
              await PreferenceHelper.removePersonalToken();

              await Navigator.pushReplacementNamed(
                context,
                SplashPage.routeName,
              );
            }
            return;
          }

          setState(() {
            _currentPageIndex = index;
            _pageController.animateToPage(
              _currentPageIndex,
              duration: const Duration(milliseconds: 400),
              curve: Curves.easeInOut,
            );
          });
        },
        items: bottomNavItems,
      ),
    );
  }
}
