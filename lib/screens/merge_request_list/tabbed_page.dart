import 'package:flutter/material.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/screens/merge_request_list/page.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class TabbedMergeRequestListPage extends StatefulWidget {
  @override
  _TabbedMergeRequestListPageState createState() =>
      _TabbedMergeRequestListPageState();
}

class _TabbedMergeRequestListPageState extends State<TabbedMergeRequestListPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);

    return DefaultTabController(
      length: 4,
      child: NestedScrollView(
        headerSliverBuilder: (context, _) {
          return [
            SliverAppBar(
              floating: true,
              pinned: true,
              leading: const Icon(MdiIcons.gitlab),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('GitLab Companion'),
                  Text(
                    'Merge Requests',
                    style: Theme.of(context).textTheme.caption,
                  ),
                ],
              ),
              bottom: TabBar(
                tabs: [
                  Tab(text: 'Open'),
                  Tab(text: 'Merged'),
                  Tab(text: 'Closed'),
                  Tab(text: 'All'),
                ],
              ),
            ),
          ];
        },
        body: TabBarView(
          children: [
            MergeRequestListPage(state: models.MergeRequestState.opened),
            MergeRequestListPage(state: models.MergeRequestState.merged),
            MergeRequestListPage(state: models.MergeRequestState.closed),
            MergeRequestListPage(),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
