import 'package:flutter/material.dart';
import 'package:gitlab_companion/components/index.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class MergeRequestListItem extends StatelessWidget {
  final String title;
  final bool hasConflicts;
  final List<models.Label> labels;
  final models.TimeStats timeStats;
  final String sourceBranch;
  final String targetBranch;
  final String reference;
  final String avatarUrl;

  const MergeRequestListItem({
    Key key,
    @required this.title,
    this.hasConflicts = false,
    this.labels = const [],
    @required this.sourceBranch,
    @required this.targetBranch,
    this.timeStats,
    @required this.reference,
    this.avatarUrl,
  })  : assert(title != null),
        assert(sourceBranch != null),
        assert(targetBranch != null),
        assert(reference != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    title,
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                if (hasConflicts) ...[
                  SizedBox(width: 16.0),
                  const Icon(
                    Icons.warning,
                    color: Colors.red,
                    size: 16.0,
                  ),
                ]
              ],
            ),
            if (labels.isNotEmpty) ...[
              SizedBox(height: 16.0),
              LabelsBox(labels: labels)
            ],
            SizedBox(height: 16.0),
            TimeStatsBox(timeStats: timeStats),
            SizedBox(height: 16.0),
            Marquee(
              child: Row(
                children: [
                  Icon(
                    MdiIcons.sourceBranch,
                    size: Theme.of(context).textTheme.bodyText2.fontSize,
                    color: Theme.of(context).textTheme.bodyText2.color,
                  ),
                  SizedBox(width: 4.0),
                  Text(
                    sourceBranch,
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                  SizedBox(width: 16.0),
                  Icon(
                    MdiIcons.sourceMerge,
                    size: Theme.of(context).textTheme.bodyText2.fontSize,
                    color: Theme.of(context).textTheme.bodyText2.color,
                  ),
                  SizedBox(width: 4.0),
                  Text(
                    targetBranch,
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ],
              ),
            ),
            SizedBox(height: 16.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  reference,
                  style: Theme.of(context).textTheme.caption,
                ),
                if ((avatarUrl ?? '').isNotEmpty)
                  SizedBox(
                    width: 24.0,
                    height: 24.0,
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(avatarUrl),
                    ),
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
