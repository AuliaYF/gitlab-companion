part of 'merge_request_list_bloc.dart';

abstract class MergeRequestListEvent extends Equatable {
  const MergeRequestListEvent();
}

class MergeRequestListLoaded extends MergeRequestListEvent {
  final bool refresh;
  final int page;

  MergeRequestListLoaded({this.refresh = false, this.page = 1});

  @override
  List<Object> get props => [refresh, page];
}
