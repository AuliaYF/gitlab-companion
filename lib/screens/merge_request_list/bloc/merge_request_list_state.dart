part of 'merge_request_list_bloc.dart';

abstract class MergeRequestListState extends Equatable {
  const MergeRequestListState();
}

class MergeRequestListInitial extends MergeRequestListState {
  @override
  List<Object> get props => [];
}

class MergeRequestListInProgress extends MergeRequestListState {
  @override
  List<Object> get props => [];
}

class MergeRequestListFailure extends MergeRequestListState {
  final String errorMessage;

  MergeRequestListFailure({this.errorMessage});

  @override
  List<Object> get props => [errorMessage];
}

class MergeRequestListSuccess extends MergeRequestListState {
  final bool hasError;
  final String errorMessage;
  final bool hasMore;
  final int nextPage;
  final List<models.MergeRequest> mergeRequests;

  MergeRequestListSuccess({
    this.hasError = false,
    this.errorMessage,
    this.hasMore = false,
    this.nextPage,
    this.mergeRequests,
  });

  @override
  List<Object> get props => [
        hasError,
        errorMessage,
        hasMore,
        nextPage,
        mergeRequests,
      ];
}
