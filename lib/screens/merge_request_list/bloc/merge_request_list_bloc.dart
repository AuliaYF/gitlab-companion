import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/services/index.dart';
import 'package:rxdart/rxdart.dart';

part 'merge_request_list_event.dart';
part 'merge_request_list_state.dart';

class MergeRequestListBloc
    extends Bloc<MergeRequestListEvent, MergeRequestListState> {
  final MergeRequestService _service;
  final models.MergeRequestState _state;

  MergeRequestListBloc(this._service, {models.MergeRequestState state})
      : _state = state;

  @override
  Stream<Transition<MergeRequestListEvent, MergeRequestListState>>
      transformEvents(Stream<MergeRequestListEvent> events, transitionFn) {
    return super.transformEvents(
      events.debounceTime(Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  MergeRequestListState get initialState => MergeRequestListInitial();

  @override
  Stream<MergeRequestListState> mapEventToState(
    MergeRequestListEvent event,
  ) async* {
    if (event is MergeRequestListLoaded) {
      if (event.refresh) {
        yield MergeRequestListInitial();
      }

      var mergeRequests = <models.MergeRequest>[];

      if (state is MergeRequestListSuccess) {
        mergeRequests.addAll((state as MergeRequestListSuccess).mergeRequests);
      }

      yield MergeRequestListInProgress();

      try {
        final response = await _service.fetchMRs(
          page: event.page,
          state: _state,
        );

        mergeRequests.addAll(response.data);

        yield MergeRequestListSuccess(
          hasMore: response.hasMore,
          nextPage: response.nextPage,
          mergeRequests: mergeRequests,
        );
      } catch (e) {
        if (event.refresh) {
          yield MergeRequestListFailure(errorMessage: e.toString());
        } else {
          yield MergeRequestListSuccess(
            hasError: true,
            errorMessage: e.toString(),
            mergeRequests: mergeRequests,
          );
        }
      }
    }
  }
}
