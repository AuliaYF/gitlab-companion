part of 'page.dart';

class MergeRequestListScreen extends StatefulWidget {
  @override
  _MergeRequestListScreenState createState() => _MergeRequestListScreenState();
}

class _MergeRequestListScreenState extends State<MergeRequestListScreen> {
  Completer _completer = Completer();
  bool _hasMore = false;
  int _nextPage = 1;

  void _load({bool refresh = false}) {
    final bloc = BlocProvider.of<MergeRequestListBloc>(context);

    bloc.add(MergeRequestListLoaded(refresh: refresh, page: _nextPage));
  }

  Future<void> _onRefresh() {
    _nextPage = 1;
    _load(refresh: true);

    return _completer.future;
  }

  @override
  void initState() {
    super.initState();

    _load(refresh: true);
  }

  @override
  void dispose() {
    _completer?.complete();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<MergeRequestListBloc>(context);

    return BlocListener<MergeRequestListBloc, MergeRequestListState>(
      listener: (context, state) {
        if (state is MergeRequestListSuccess && state.hasError) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(state.errorMessage),
              backgroundColor: Colors.red,
            ),
          );
        }

        if (state is MergeRequestListSuccess && !state.hasError) {
          _hasMore = state.hasMore;
          _nextPage = state.nextPage;
        }

        if (state is MergeRequestListSuccess ||
            state is MergeRequestListFailure) {
          _completer?.complete();

          _completer = Completer();
        }
      },
      child: RefreshIndicator(
        onRefresh: _onRefresh,
        child: ReachedBottomListener(
          onReached: () {
            if (!(bloc.state is MergeRequestListInProgress) && _hasMore) {
              _load();
            }
          },
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(),
              BlocBuilder<MergeRequestListBloc, MergeRequestListState>(
                condition: (prevState, nextState) {
                  return !(nextState is MergeRequestListInProgress);
                },
                builder: (context, state) {
                  if (state is MergeRequestListFailure) {
                    return SliverFillRemaining(
                      child: ErrorBox(
                        errorMessage: state.errorMessage,
                        onRetry: _onRefresh,
                      ),
                    );
                  } else if (state is MergeRequestListSuccess) {
                    if (state.mergeRequests.isNotEmpty) {
                      return SliverPadding(
                        padding: const EdgeInsets.all(16.0),
                        sliver: SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (context, index) {
                              if (index == state.mergeRequests.length) {
                                return Padding(
                                  padding: const EdgeInsets.all(16.0).copyWith(
                                    bottom: 0.0,
                                  ),
                                  child: Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                );
                              }

                              var mergeRequest =
                                  state.mergeRequests.elementAt(index);

                              return Column(
                                children: [
                                  MergeRequestListItem(
                                    title: mergeRequest.title,
                                    hasConflicts: mergeRequest.hasConflicts,
                                    labels: mergeRequest.labels,
                                    timeStats: mergeRequest.timeStats,
                                    sourceBranch: mergeRequest.sourceBranch,
                                    targetBranch: mergeRequest.targetBranch,
                                    reference: mergeRequest.references.full,
                                    avatarUrl: mergeRequest.assignee?.avatarUrl,
                                  ),
                                  if (index < state.mergeRequests.length - 1)
                                    SizedBox(height: 16.0),
                                ],
                              );
                            },
                            childCount: state.mergeRequests.length +
                                (state.hasMore ? 1 : 0),
                          ),
                        ),
                      );
                    } else {
                      return SliverFillRemaining(
                        child: InfoBox(
                          message: 'No merge request available',
                        ),
                      );
                    }
                  }

                  return SliverFillRemaining(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
