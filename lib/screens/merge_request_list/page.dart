import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gitlab_companion/components/index.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/screens/merge_request_list/bloc/merge_request_list_bloc.dart';
import 'package:gitlab_companion/screens/merge_request_list/components/index.dart';
import 'package:gitlab_companion/services/index.dart';
import 'package:provider/provider.dart';

part 'screen.dart';

class MergeRequestListPage extends StatefulWidget {
  final models.MergeRequestState state;

  const MergeRequestListPage({Key key, this.state}) : super(key: key);

  @override
  _MergeRequestListPageState createState() => _MergeRequestListPageState();
}

class _MergeRequestListPageState extends State<MergeRequestListPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);

    final dio = Provider.of<Dio>(context);
    final service = MergeRequestService(dio);

    return BlocProvider<MergeRequestListBloc>(
      create: (context) => MergeRequestListBloc(service, state: widget.state),
      child: MergeRequestListScreen(),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
