import 'package:flutter/material.dart';
import 'package:gitlab_companion/models/index.dart' as models;

class TimeStatsBox extends StatelessWidget {
  final models.TimeStats timeStats;

  const TimeStatsBox({Key key, @required this.timeStats})
      : assert(timeStats != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text('Time Tracking'),
        SizedBox(height: 8.0),
        if (timeStats.timeEstimate == 0 && timeStats.totalTimeSpent == 0)
          Text(
            'No estimate or time spent',
            style: Theme.of(context).textTheme.caption,
          ),
        if (timeStats.timeEstimate > 0 || timeStats.totalTimeSpent > 0) ...[
          LinearProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              timeStats.totalTimeSpent > timeStats.timeEstimate &&
                      timeStats.timeEstimate > 0
                  ? Colors.red
                  : null,
            ),
            value: (timeStats.totalTimeSpent > 0
                    ? (timeStats.timeEstimate > 0
                        ? (timeStats.totalTimeSpent > timeStats.timeEstimate
                            ? timeStats.timeEstimate
                            : (timeStats.totalTimeSpent /
                                timeStats.timeEstimate))
                        : 1.0)
                    : 0.0)
                .toDouble(),
          ),
          SizedBox(height: 8.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(text: 'Spent '),
                    TextSpan(
                      text: timeStats.humanTotalTimeSpent,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color:
                            timeStats.totalTimeSpent > timeStats.timeEstimate &&
                                    timeStats.timeEstimate > 0
                                ? Colors.red
                                : null,
                      ),
                    )
                  ],
                  style: Theme.of(context).textTheme.caption,
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(text: 'Est '),
                    TextSpan(
                      text: timeStats.humanTimeEstimate,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                      ),
                    )
                  ],
                  style: Theme.of(context).textTheme.caption,
                ),
              )
            ],
          ),
        ],
      ],
    );
  }
}
