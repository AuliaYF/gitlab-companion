import 'package:flutter/material.dart';

class ReachedBottomListener extends StatelessWidget {
  final Function onReached;
  final Widget child;

  const ReachedBottomListener({
    Key key,
    @required this.onReached,
    @required this.child,
  })  : assert(onReached != null),
        assert(child != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
      onNotification: (notification) {
        final metrics = notification.metrics;

        if (metrics.pixels == metrics.maxScrollExtent) {
          onReached();
        }

        return false;
      },
      child: child,
    );
  }
}
