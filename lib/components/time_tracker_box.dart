import 'package:flutter/material.dart';
import 'package:gitlab_companion/components/index.dart';
import 'package:gitlab_companion/models/index.dart' as models;

class TimeTrackerBox extends StatelessWidget {
  final models.TimeStats timeStats;
  final bool syncing;
  final Function(models.TimeStats) onStarted;
  final Function(models.TimeStats) onStopped;
  final Function(models.TimeStats) onSynced;
  final Function(models.TimeStats) onRemoved;

  const TimeTrackerBox({
    Key key,
    @required this.timeStats,
    this.syncing = false,
    this.onStarted,
    this.onStopped,
    this.onSynced,
    this.onRemoved,
  })  : assert(timeStats != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      shape: CircleBorder(),
      child: Container(
        padding: const EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: Text(
                  'Time Spent',
                  style: Theme.of(context).textTheme.caption,
                ),
              ),
            ),
            Center(
              child: Text(
                timeStats.humanTotalTimeSpent,
                style: Theme.of(context).textTheme.headline5,
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Material(
                color: Colors.transparent,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (timeStats.running)
                        IconButton(
                          onPressed: () {
                            if (onStopped != null) {
                              onStopped(timeStats);
                            }
                          },
                          icon: Icon(Icons.stop),
                          tooltip: 'Stop',
                        ),
                      if (!timeStats.running)
                        IconButton(
                          onPressed: () {
                            if (onStarted != null) {
                              onStarted(timeStats);
                            }
                          },
                          icon: Icon(Icons.play_arrow),
                          tooltip: 'Start',
                        ),
                      IconButton(
                        onPressed: timeStats.running ||
                                onSynced == null ||
                                timeStats.synced
                            ? null
                            : () {
                                onSynced(timeStats);
                              },
                        icon: syncing
                            ? RotatingIcon(icon: const Icon(Icons.refresh))
                            : const Icon(Icons.refresh),
                        tooltip: 'Sync',
                      ),
                      IconButton(
                        onPressed: timeStats.running ||
                                onRemoved == null ||
                                timeStats.synced
                            ? null
                            : () {
                                onRemoved(timeStats);
                              },
                        icon: const Icon(Icons.delete),
                        tooltip: 'Remove',
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
