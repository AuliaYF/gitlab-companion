import 'package:flutter/material.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:timeago/timeago.dart' as timeago;

enum NotePosition { first, middle, last }

class NoteListItem extends StatelessWidget {
  const NoteListItem({
    this.noLine = false,
    this.position = NotePosition.first,
    @required this.author,
    @required this.body,
    @required this.dateTime,
    Key key,
  })  : assert(author != null),
        assert(body != null),
        assert(dateTime != null),
        super(key: key);

  final bool noLine;
  final NotePosition position;
  final models.User author;
  final String body;
  final DateTime dateTime;

  @override
  Widget build(BuildContext context) {
    var icon = MdiIcons.messageProcessingOutline;

    if (body.contains('assigned')) {
      icon = MdiIcons.accountOutline;
    } else if (body.contains('time estimate') || body.contains('time spent')) {
      icon = MdiIcons.timerOutline;
    } else if (body.contains('due date')) {
      icon = MdiIcons.calendarBlankOutline;
    } else if (body.contains('closed')) {
      icon = MdiIcons.minusCircleOutline;
    } else if (body.contains('locked')) {
      icon = MdiIcons.lockOutline;
    }

    return IntrinsicHeight(
      child: Row(
        children: [
          Column(
            children: [
              if (position == NotePosition.first || noLine)
                Expanded(child: SizedBox()),
              if ([NotePosition.middle, NotePosition.last].contains(position) &&
                  !noLine)
                Expanded(
                  child: Container(
                    width: 1.0,
                    color: Theme.of(context).disabledColor,
                  ),
                ),
              Container(
                padding: const EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Theme.of(context).disabledColor,
                  ),
                  borderRadius: BorderRadius.circular(16.0),
                ),
                child: Icon(
                  icon,
                  size: 16.0,
                  color: Theme.of(context).disabledColor,
                ),
              ),
              if (position == NotePosition.last || noLine)
                Expanded(child: SizedBox()),
              if ([NotePosition.first, NotePosition.middle]
                      .contains(position) &&
                  !noLine)
                Expanded(
                  child: Container(
                    width: 1.0,
                    color: Theme.of(context).disabledColor,
                  ),
                ),
            ],
          ),
          SizedBox(width: 16.0),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: author.name,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(text: ' '),
                        TextSpan(text: '@${author.username}'),
                        TextSpan(text: ' '),
                        TextSpan(text: body),
                      ],
                    ),
                  ),
                  SizedBox(height: 4.0),
                  Text(
                    timeago.format(dateTime),
                    style: Theme.of(context).textTheme.caption,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
