import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:gitlab_companion/components/index.dart';
import 'package:gitlab_companion/helpers/index.dart';
import 'package:gitlab_companion/models/index.dart' as models;

class LabelsBox extends StatelessWidget {
  const LabelsBox({
    Key key,
    @required this.labels,
  }) : super(key: key);

  final List<models.Label> labels;

  @override
  Widget build(BuildContext context) {
    return Marquee(
      child: Row(
        children: labels.map((label) {
          return Row(
            children: [
              Badge(
                toAnimate: false,
                shape: BadgeShape.square,
                elevation: 0.0,
                badgeColor: StringHelper.toColor(
                  label.backgroundColor,
                ),
                borderRadius: 16.0,
                badgeContent: Text(
                  label.name,
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: StringHelper.toColor(label.textColor)),
                ),
              ),
              if (label != labels.last) SizedBox(width: 8.0),
            ],
          );
        }).toList(),
      ),
    );
  }
}
