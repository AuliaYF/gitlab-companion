import 'package:flutter/material.dart';

class RotatingIcon extends StatefulWidget {
  final Duration duration;
  final Icon icon;

  const RotatingIcon({Key key, this.duration, @required this.icon})
      : assert(icon != null),
        super(key: key);

  @override
  _RotatingIconState createState() => _RotatingIconState();
}

class _RotatingIconState extends State<RotatingIcon>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: widget.duration ?? Duration(seconds: 3),
    );
    _controller.repeat();
  }

  @override
  void dispose() {
    _controller?.reset();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
      turns: Tween(begin: 0.0, end: 1.0).animate(_controller),
      child: widget.icon,
    );
  }
}
