import 'package:flutter/material.dart';

class InfoBox extends StatelessWidget {
  const InfoBox({
    Key key,
    @required this.message,
  })  : assert(message != null),
        super(key: key);

  final String message;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Icon(
          Icons.folder_open,
          size: 96.0,
        ),
        SizedBox(height: 16.0),
        Text(message, textAlign: TextAlign.center),
      ],
    );
  }
}
