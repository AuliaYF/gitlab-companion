import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:gitlab_companion/models/index.dart' as models;

class NoteFormBox extends StatelessWidget {
  const NoteFormBox({
    Key key,
    @required this.user,
  })  : assert(user != null),
        super(key: key);

  final models.User user;

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: 32.0,
            height: 32.0,
            child: CircleAvatar(
              backgroundImage: NetworkImage(
                user.avatarUrl,
              ),
            ),
          ),
          SizedBox(width: 16.0),
          Expanded(
            child: _FormBox(),
          ),
        ],
      ),
    );
  }
}

class _FormBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        FormBuilderTextField(
          attribute: 'note',
          minLines: 5,
          decoration: InputDecoration(
            filled: true,
            hintText: 'Write a comment here...',
          ),
          validators: [
            FormBuilderValidators.required(),
          ],
        ),
        SizedBox(height: 12.0),
        RaisedButton(
          onPressed: () {},
          child: const Text('COMMENT'),
        )
      ],
    );
  }
}
