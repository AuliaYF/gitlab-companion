import 'package:flutter/material.dart';

class ErrorBox extends StatelessWidget {
  const ErrorBox({
    Key key,
    this.showIcon = true,
    @required this.errorMessage,
    this.onRetry,
  })  : assert(errorMessage != null),
        super(key: key);

  final bool showIcon;
  final String errorMessage;
  final Function onRetry;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        if (showIcon) ...[
          const Icon(
            Icons.error_outline,
            size: 96.0,
          ),
          SizedBox(height: 16.0),
        ],
        Text(errorMessage, textAlign: TextAlign.center),
        SizedBox(height: 12.0),
        OutlineButton(
          onPressed: onRetry,
          child: const Text('RETRY'),
        ),
      ],
    );
  }
}
