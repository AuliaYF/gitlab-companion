import 'package:flutter/material.dart';
import 'package:gitlab_companion/models/index.dart' as models;

class Session with ChangeNotifier {
  models.User _currentUser;

  set currentUser(models.User user) {
    _currentUser = user;

    notifyListeners();
  }

  models.User get currentUser => _currentUser;
}
