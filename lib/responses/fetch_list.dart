class FetchListResponse<T> {
  final bool hasMore;
  final int nextPage;
  final List<T> data;

  FetchListResponse({
    this.hasMore = false,
    this.nextPage,
    this.data = const [],
  });
}
