import 'package:json_annotation/json_annotation.dart';

part 'milestone.g.dart';

enum MilestoneState { active, closed }

@JsonSerializable()
class Milestone {
  @JsonKey(required: true)
  final int id;
  final String title;
  final String description;
  final MilestoneState state;
  @JsonKey(name: 'created_at')
  final DateTime createdAt;
  @JsonKey(name: 'updated_at')
  final DateTime updatedAt;
  @JsonKey(name: 'due_date')
  final DateTime dueDate;
  @JsonKey(name: 'start_date')
  final DateTime startDate;
  @JsonKey(name: 'web_url')
  final String webUrl;

  Milestone({
    this.id,
    this.title,
    this.description,
    this.state,
    this.createdAt,
    this.updatedAt,
    this.dueDate,
    this.startDate,
    this.webUrl,
  });

  factory Milestone.fromJson(Map<String, dynamic> json) =>
      _$MilestoneFromJson(json);
  Map<String, dynamic> toJson() => _$MilestoneToJson(this);
}
