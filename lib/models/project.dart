import 'package:json_annotation/json_annotation.dart';

part 'project.g.dart';

@JsonSerializable()
class Project {
  @JsonKey(required: true)
  final int id;
  final String name;
  @JsonKey(name: 'name_with_namespace')
  final String nameWithNamespace;
  final String description;
  final String path;
  @JsonKey(name: 'path_with_namespace')
  final String pathWithNamespace;
  @JsonKey(name: 'created_at')
  final DateTime createdAt;
  @JsonKey(name: 'web_url')
  final String webUrl;
  @JsonKey(name: 'star_count', defaultValue: 0)
  final int totalStars;
  @JsonKey(name: 'forks_count', defaultValue: 0)
  final int totalForks;
  @JsonKey(name: 'open_issues_count', defaultValue: 0)
  final int totalIssues;

  Project({
    this.id,
    this.name,
    this.nameWithNamespace,
    this.description,
    this.path,
    this.pathWithNamespace,
    this.createdAt,
    this.webUrl,
    this.totalStars,
    this.totalForks,
    this.totalIssues,
  });

  factory Project.fromJson(Map<String, dynamic> json) =>
      _$ProjectFromJson(json);
  Map<String, dynamic> toJson() => _$ProjectToJson(this);
}
