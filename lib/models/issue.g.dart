// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'issue.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Issue _$IssueFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['id']);
  return Issue(
    id: json['id'] as int,
    iid: json['iid'] as int,
    projectId: json['project_id'] as int,
    title: json['title'] as String,
    description: json['description'] as String,
    state: _$enumDecodeNullable(_$IssueStateEnumMap, json['state']),
    createdAt: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    updatedAt: json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String),
    closedAt: json['closed_at'] == null
        ? null
        : DateTime.parse(json['closed_at'] as String),
    labels: (json['labels'] as List)
            ?.map((e) =>
                e == null ? null : Label.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    milestone: json['milestone'] == null
        ? null
        : Milestone.fromJson(json['milestone'] as Map<String, dynamic>),
    assignees: (json['assignees'] as List)
        ?.map(
            (e) => e == null ? null : User.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    author: json['author'] == null
        ? null
        : User.fromJson(json['author'] as Map<String, dynamic>),
    assignee: json['assignee'] == null
        ? null
        : User.fromJson(json['assignee'] as Map<String, dynamic>),
    dueDate: json['due_date'] == null
        ? null
        : DateTime.parse(json['due_date'] as String),
    webUrl: json['web_url'] as String,
    timeStats: json['time_stats'] == null
        ? null
        : TimeStats.fromJson(json['time_stats'] as Map<String, dynamic>),
    references: json['references'] == null
        ? null
        : References.fromJson(json['references'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$IssueToJson(Issue instance) => <String, dynamic>{
      'id': instance.id,
      'iid': instance.iid,
      'project_id': instance.projectId,
      'title': instance.title,
      'description': instance.description,
      'state': _$IssueStateEnumMap[instance.state],
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'closed_at': instance.closedAt?.toIso8601String(),
      'labels': instance.labels,
      'milestone': instance.milestone,
      'assignees': instance.assignees,
      'author': instance.author,
      'assignee': instance.assignee,
      'due_date': instance.dueDate?.toIso8601String(),
      'web_url': instance.webUrl,
      'time_stats': instance.timeStats,
      'references': instance.references,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$IssueStateEnumMap = {
  IssueState.opened: 'opened',
  IssueState.closed: 'closed',
};
