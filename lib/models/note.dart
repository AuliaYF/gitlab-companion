import 'package:gitlab_companion/models/index.dart';
import 'package:json_annotation/json_annotation.dart';

part 'note.g.dart';

@JsonSerializable()
class Note {
  @JsonKey(required: true)
  final int id;
  final String body;
  final User author;
  @JsonKey(name: 'created_at')
  final DateTime createdAt;
  @JsonKey(name: 'updated_at')
  final DateTime updatedAt;
  @JsonKey(defaultValue: false)
  final bool system;

  Note({
    this.id,
    this.body,
    this.author,
    this.createdAt,
    this.updatedAt,
    this.system,
  });

  factory Note.fromJson(Map<String, dynamic> json) => _$NoteFromJson(json);
  Map<String, dynamic> toJson() => _$NoteToJson(this);
}
