// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'label.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Label _$LabelFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['id']);
  return Label(
    id: json['id'] as int,
    name: json['name'] as String,
    backgroundColor: json['color'] as String,
    textColor: json['text_color'] as String,
    description: json['description'] as String,
  );
}

Map<String, dynamic> _$LabelToJson(Label instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'color': instance.backgroundColor,
      'text_color': instance.textColor,
      'description': instance.description,
    };
