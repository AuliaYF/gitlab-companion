// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'time_stats.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TimeStats _$TimeStatsFromJson(Map<String, dynamic> json) {
  return TimeStats(
    timeEstimate: json['time_estimate'] as int ?? 0,
    totalTimeSpent: json['total_time_spent'] as int ?? 0,
    humanTimeEstimate: json['human_time_estimate'] as String ?? '0h',
    humanTotalTimeSpent: json['human_total_time_spent'] as String ?? '0h',
    running: json['running'] as bool ?? false,
    updatedAt: json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String),
    synced: json['synced'] as bool ?? true,
  );
}

Map<String, dynamic> _$TimeStatsToJson(TimeStats instance) => <String, dynamic>{
      'time_estimate': instance.timeEstimate,
      'total_time_spent': instance.totalTimeSpent,
      'human_time_estimate': instance.humanTimeEstimate,
      'human_total_time_spent': instance.humanTotalTimeSpent,
      'running': instance.running,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'synced': instance.synced,
    };
