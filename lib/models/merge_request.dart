import 'package:gitlab_companion/models/index.dart';
import 'package:json_annotation/json_annotation.dart';

part 'merge_request.g.dart';

enum MergeRequestState { opened, closed, locked, merged }
const Map<MergeRequestState, String> MergeRequestStateValues = {
  MergeRequestState.opened: 'opened',
  MergeRequestState.closed: 'closed',
  MergeRequestState.locked: 'locked',
  MergeRequestState.merged: 'merged',
};

@JsonSerializable()
class MergeRequest {
  @JsonKey(required: true)
  final int id;
  final String title;
  final String description;
  final MergeRequestState state;
  @JsonKey(name: 'created_at')
  final DateTime createdAt;
  @JsonKey(name: 'updated_at')
  final DateTime updatedAt;
  @JsonKey(name: 'merged_at')
  final DateTime mergedAt;
  @JsonKey(name: 'closed_at')
  final DateTime closedAt;
  @JsonKey(name: 'target_branch')
  final String targetBranch;
  @JsonKey(name: 'source_branch')
  final String sourceBranch;
  final User assignee;
  final User author;
  @JsonKey(defaultValue: [])
  final List<User> assignees;
  @JsonKey(defaultValue: [])
  final List<Label> labels;
  final References references;
  @JsonKey(name: 'web_url')
  final String webUrl;
  @JsonKey(name: 'time_stats')
  final TimeStats timeStats;
  @JsonKey(name: 'has_conflicts', defaultValue: false)
  final bool hasConflicts;

  MergeRequest({
    this.id,
    this.title,
    this.description,
    this.state,
    this.createdAt,
    this.updatedAt,
    this.mergedAt,
    this.closedAt,
    this.targetBranch,
    this.sourceBranch,
    this.assignee,
    this.author,
    this.assignees,
    this.labels,
    this.references,
    this.webUrl,
    this.timeStats,
    this.hasConflicts,
  });

  factory MergeRequest.fromJson(Map<String, dynamic> json) =>
      _$MergeRequestFromJson(json);
  Map<String, dynamic> toJson() => _$MergeRequestToJson(this);
}
