// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'project.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Project _$ProjectFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['id']);
  return Project(
    id: json['id'] as int,
    name: json['name'] as String,
    nameWithNamespace: json['name_with_namespace'] as String,
    description: json['description'] as String,
    path: json['path'] as String,
    pathWithNamespace: json['path_with_namespace'] as String,
    createdAt: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    webUrl: json['web_url'] as String,
    totalStars: json['star_count'] as int ?? 0,
    totalForks: json['forks_count'] as int ?? 0,
    totalIssues: json['open_issues_count'] as int ?? 0,
  );
}

Map<String, dynamic> _$ProjectToJson(Project instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'name_with_namespace': instance.nameWithNamespace,
      'description': instance.description,
      'path': instance.path,
      'path_with_namespace': instance.pathWithNamespace,
      'created_at': instance.createdAt?.toIso8601String(),
      'web_url': instance.webUrl,
      'star_count': instance.totalStars,
      'forks_count': instance.totalForks,
      'open_issues_count': instance.totalIssues,
    };
