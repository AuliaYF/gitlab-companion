import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  @JsonKey(required: true)
  final int id;
  final String name;
  final String username;
  @JsonKey(name: 'avatar_url')
  final String avatarUrl;
  @JsonKey(name: 'web_url')
  final String webUrl;

  User({this.id, this.name, this.username, this.avatarUrl, this.webUrl});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
