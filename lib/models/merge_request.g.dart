// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'merge_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MergeRequest _$MergeRequestFromJson(Map<String, dynamic> json) {
  $checkKeys(json, requiredKeys: const ['id']);
  return MergeRequest(
    id: json['id'] as int,
    title: json['title'] as String,
    description: json['description'] as String,
    state: _$enumDecodeNullable(_$MergeRequestStateEnumMap, json['state']),
    createdAt: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    updatedAt: json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String),
    mergedAt: json['merged_at'] == null
        ? null
        : DateTime.parse(json['merged_at'] as String),
    closedAt: json['closed_at'] == null
        ? null
        : DateTime.parse(json['closed_at'] as String),
    targetBranch: json['target_branch'] as String,
    sourceBranch: json['source_branch'] as String,
    assignee: json['assignee'] == null
        ? null
        : User.fromJson(json['assignee'] as Map<String, dynamic>),
    author: json['author'] == null
        ? null
        : User.fromJson(json['author'] as Map<String, dynamic>),
    assignees: (json['assignees'] as List)
            ?.map((e) =>
                e == null ? null : User.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    labels: (json['labels'] as List)
            ?.map((e) =>
                e == null ? null : Label.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    references: json['references'] == null
        ? null
        : References.fromJson(json['references'] as Map<String, dynamic>),
    webUrl: json['web_url'] as String,
    timeStats: json['time_stats'] == null
        ? null
        : TimeStats.fromJson(json['time_stats'] as Map<String, dynamic>),
    hasConflicts: json['has_conflicts'] as bool ?? false,
  );
}

Map<String, dynamic> _$MergeRequestToJson(MergeRequest instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'state': _$MergeRequestStateEnumMap[instance.state],
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'merged_at': instance.mergedAt?.toIso8601String(),
      'closed_at': instance.closedAt?.toIso8601String(),
      'target_branch': instance.targetBranch,
      'source_branch': instance.sourceBranch,
      'assignee': instance.assignee,
      'author': instance.author,
      'assignees': instance.assignees,
      'labels': instance.labels,
      'references': instance.references,
      'web_url': instance.webUrl,
      'time_stats': instance.timeStats,
      'has_conflicts': instance.hasConflicts,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$MergeRequestStateEnumMap = {
  MergeRequestState.opened: 'opened',
  MergeRequestState.closed: 'closed',
  MergeRequestState.locked: 'locked',
  MergeRequestState.merged: 'merged',
};
