import 'package:json_annotation/json_annotation.dart';

part 'label.g.dart';

@JsonSerializable()
class Label {
  @JsonKey(required: true)
  final int id;
  final String name;
  @JsonKey(name: 'color')
  final String backgroundColor;
  @JsonKey(name: 'text_color')
  final String textColor;
  final String description;

  Label({
    this.id,
    this.name,
    this.backgroundColor,
    this.textColor,
    this.description,
  });

  factory Label.fromJson(Map<String, dynamic> json) => _$LabelFromJson(json);
  Map<String, dynamic> toJson() => _$LabelToJson(this);
}
