import 'package:gitlab_companion/models/index.dart';
import 'package:json_annotation/json_annotation.dart';

part 'issue.g.dart';

enum IssueState { opened, closed }

const Map<IssueState, String> IssueStateValues = {
  IssueState.opened: 'opened',
  IssueState.closed: 'closed',
};

@JsonSerializable()
class Issue {
  @JsonKey(required: true)
  final int id;
  final int iid;
  @JsonKey(name: 'project_id')
  final int projectId;
  final String title;
  final String description;
  final IssueState state;
  @JsonKey(name: 'created_at')
  final DateTime createdAt;
  @JsonKey(name: 'updated_at')
  final DateTime updatedAt;
  @JsonKey(name: 'closed_at')
  final DateTime closedAt;
  @JsonKey(defaultValue: [])
  final List<Label> labels;
  final Milestone milestone;
  final List<User> assignees;
  final User author;
  final User assignee;
  @JsonKey(name: 'due_date')
  final DateTime dueDate;
  @JsonKey(name: 'web_url')
  final String webUrl;
  @JsonKey(name: 'time_stats')
  final TimeStats timeStats;
  final References references;

  Issue({
    this.id,
    this.iid,
    this.projectId,
    this.title,
    this.description,
    this.state,
    this.createdAt,
    this.updatedAt,
    this.closedAt,
    this.labels,
    this.milestone,
    this.assignees,
    this.author,
    this.assignee,
    this.dueDate,
    this.webUrl,
    this.timeStats,
    this.references,
  });

  Issue newTimeStats(TimeStats timeStats) {
    return Issue(
      id: id,
      iid: iid,
      projectId: projectId,
      title: title,
      description: description,
      state: state,
      createdAt: createdAt,
      updatedAt: updatedAt,
      closedAt: closedAt,
      labels: labels,
      milestone: milestone,
      assignees: assignees,
      author: author,
      assignee: assignee,
      dueDate: dueDate,
      webUrl: webUrl,
      timeStats: timeStats,
      references: references,
    );
  }

  factory Issue.fromJson(Map<String, dynamic> json) => _$IssueFromJson(json);
  Map<String, dynamic> toJson() => _$IssueToJson(this);
}
