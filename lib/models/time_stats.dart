import 'package:json_annotation/json_annotation.dart';

part 'time_stats.g.dart';

@JsonSerializable()
class TimeStats {
  @JsonKey(name: 'time_estimate', defaultValue: 0)
  final int timeEstimate;
  @JsonKey(name: 'total_time_spent', defaultValue: 0)
  final int totalTimeSpent;
  @JsonKey(name: 'human_time_estimate', defaultValue: '0h')
  final String humanTimeEstimate;
  @JsonKey(name: 'human_total_time_spent', defaultValue: '0h')
  final String humanTotalTimeSpent;
  @JsonKey(defaultValue: false)
  final bool running;
  @JsonKey(name: 'updated_at')
  final DateTime updatedAt;
  @JsonKey(defaultValue: true)
  final bool synced;

  TimeStats({
    this.timeEstimate,
    this.totalTimeSpent,
    this.humanTimeEstimate,
    this.humanTotalTimeSpent,
    this.running,
    this.updatedAt,
    this.synced,
  });

  TimeStats copyWith({
    int timeEstimate,
    int totalTimeSpent,
    String humanTimeEstimate,
    String humanTotalTimeSpent,
    bool running,
    DateTime updatedAt,
    bool synced,
  }) {
    return TimeStats(
      timeEstimate: timeEstimate ?? this.timeEstimate,
      totalTimeSpent: totalTimeSpent ?? this.totalTimeSpent,
      humanTimeEstimate: humanTimeEstimate ?? this.humanTimeEstimate,
      humanTotalTimeSpent: humanTotalTimeSpent ?? this.humanTotalTimeSpent,
      running: running ?? this.running,
      updatedAt: updatedAt ?? this.updatedAt,
      synced: synced ?? this.synced,
    );
  }

  factory TimeStats.fromJson(Map<String, dynamic> json) =>
      _$TimeStatsFromJson(json);
  Map<String, dynamic> toJson() => _$TimeStatsToJson(this);
}
