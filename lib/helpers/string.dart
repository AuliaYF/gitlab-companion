import 'package:flutter/material.dart';

class StringHelper {
  static Color toColor(String input) {
    final buffer = StringBuffer();
    if (input.length == 6 || input.length == 7) buffer.write('ff');
    buffer.write(input.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  static String fromColor(Color input) =>
      '${input.red.toRadixString(16).padLeft(2, '0')}'
      '${input.green.toRadixString(16).padLeft(2, '0')}'
      '${input.blue.toRadixString(16).padLeft(2, '0')}';
}
