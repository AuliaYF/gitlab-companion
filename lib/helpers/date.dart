import 'package:intl/intl.dart';

class DateHelper {
  static String format(DateTime dateTime, String format) {
    var formatter = DateFormat(format);

    return formatter.format(dateTime);
  }

  static bool isBeforeToday(DateTime dateTime) {
    var now = DateTime.now();
    var today = DateTime(now.year, now.month, now.day);
    var date = DateTime(dateTime.year, dateTime.month, dateTime.day);

    return date.isBefore(today);
  }

  static bool isAfterToday(DateTime dateTime) {
    var now = DateTime.now();
    var today = DateTime(now.year, now.month, now.day);
    var date = DateTime(dateTime.year, dateTime.month, dateTime.day);

    return date.isAfter(today);
  }
}
