import 'dart:convert';

import 'package:gitlab_companion/models/index.dart' as models;
import 'package:shared_preferences/shared_preferences.dart';

class PreferenceKeys {
  static final String personalToken = 'personal-token';
  static final String timeStats = 'time-stats';
}

class PreferenceHelper {
  static Future<String> getPersonalToken() async {
    var sharedPreferences = await SharedPreferences.getInstance();

    return sharedPreferences.getString(PreferenceKeys.personalToken);
  }

  static Future<bool> removePersonalToken() async {
    var sharedPreferences = await SharedPreferences.getInstance();

    return sharedPreferences.remove(PreferenceKeys.personalToken);
  }

  static Future<bool> setPersonalToken(String personalToken) async {
    var sharedPreferences = await SharedPreferences.getInstance();

    return sharedPreferences.setString(
      PreferenceKeys.personalToken,
      personalToken,
    );
  }

  static Future<bool> hasPersonalToken() async {
    var personalToken = await PreferenceHelper.getPersonalToken();

    return (personalToken ?? '').isNotEmpty;
  }

  static Future<Map<String, models.TimeStats>> getTimeStats() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    var raw = sharedPreferences.getString(PreferenceKeys.timeStats);
    var timeStats = raw == null ? {} : json.decode(raw) as Map<String, dynamic>;

    return timeStats.map((key, val) {
      return MapEntry(
        key,
        val == null
            ? null
            : models.TimeStats.fromJson(val as Map<String, dynamic>),
      );
    });
  }

  static Future<void> setTimeStats(
    Map<String, models.TimeStats> timeStats,
  ) async {
    var sharedPreferences = await SharedPreferences.getInstance();

    return sharedPreferences.setString(
      PreferenceKeys.timeStats,
      json.encode(timeStats),
    );
  }
}
