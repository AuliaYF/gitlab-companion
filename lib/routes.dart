import 'package:flutter/material.dart';
import 'package:gitlab_companion/screens/home/page.dart';
import 'package:gitlab_companion/screens/setup/page.dart';
import 'package:gitlab_companion/screens/splash/page.dart';

final Map<String, WidgetBuilder> routes = {
  SplashPage.routeName: (context) => SplashPage(),
  SetupPage.routeName: (context) => SetupPage(),
  HomePage.routeName: (context) => HomePage(),
};
