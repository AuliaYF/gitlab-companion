import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/services/index.dart';

part 'issue_container_event.dart';
part 'issue_container_state.dart';

class IssueContainerBloc
    extends Bloc<IssueContainerEvent, IssueContainerState> {
  final IssueService _service;
  final Map<String, models.Issue> _issues = {};

  IssueContainerBloc(this._service);

  @override
  IssueContainerState get initialState => IssueContainerInitial();

  @override
  Stream<IssueContainerState> mapEventToState(
    IssueContainerEvent event,
  ) async* {
    if (event is IssueContainerRegistered) {
      yield IssueContainerInProgress();

      _issues[event.issue.references.full] = event.issue;

      yield IssueContainerSuccess(issues: _issues);
    }

    if (event is IssueContainerUpdated) {
      yield IssueContainerInProgress();

      var syncFromObjectId;
      try {
        final newIssue = await _service.fetchIssue(event.projectId, event.iid);

        syncFromObjectId = newIssue.references.full;
        _issues[newIssue.references.full] =
            _issues[newIssue.references.full].newTimeStats(
          newIssue.timeStats,
        );
      } catch (_) {}

      yield IssueContainerSuccess(
        issues: _issues,
        syncFromObjectId: syncFromObjectId,
      );
    }
  }
}
