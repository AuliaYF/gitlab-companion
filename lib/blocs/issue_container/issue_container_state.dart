part of 'issue_container_bloc.dart';

abstract class IssueContainerState extends Equatable {
  const IssueContainerState();
}

class IssueContainerInitial extends IssueContainerState {
  @override
  List<Object> get props => [];
}

class IssueContainerInProgress extends IssueContainerState {
  @override
  List<Object> get props => throw UnimplementedError();
}

class IssueContainerSuccess extends IssueContainerState {
  final Map<String, models.Issue> issues;
  final String syncFromObjectId;

  IssueContainerSuccess({this.issues, this.syncFromObjectId});

  @override
  List<Object> get props => [issues, syncFromObjectId];
}
