part of 'issue_container_bloc.dart';

abstract class IssueContainerEvent extends Equatable {
  const IssueContainerEvent();
}

class IssueContainerRegistered extends IssueContainerEvent {
  final models.Issue issue;

  IssueContainerRegistered({this.issue});

  @override
  List<Object> get props => [issue];
}

class IssueContainerUpdated extends IssueContainerEvent {
  final int projectId;
  final int iid;

  IssueContainerUpdated({this.projectId, this.iid});

  @override
  List<Object> get props => [projectId, iid];
}
