part of 'time_tracker_bloc.dart';

abstract class TimeTrackerEvent extends Equatable {
  const TimeTrackerEvent();
}

class TimeTrackerStarted extends TimeTrackerEvent {
  @override
  List<Object> get props => [];
}

class TimeTrackerUpdated extends TimeTrackerEvent {
  final Map<String, models.TimeStats> timeStats;

  TimeTrackerUpdated({this.timeStats});

  @override
  List<Object> get props => [timeStats];
}

class TimeTrackerObjectStarted extends TimeTrackerEvent {
  final String objectId;
  final models.TimeStats initialTimeStats;

  TimeTrackerObjectStarted({this.objectId, this.initialTimeStats});

  @override
  List<Object> get props => [objectId, initialTimeStats];
}

class TimeTrackerObjectStopped extends TimeTrackerEvent {
  final String objectId;

  TimeTrackerObjectStopped({this.objectId});

  @override
  List<Object> get props => [objectId];
}

class TimeTrackerObjectRemoved extends TimeTrackerEvent {
  final String objectId;

  TimeTrackerObjectRemoved({this.objectId});

  @override
  List<Object> get props => [objectId];
}

class TimeTrackerIssueSynced extends TimeTrackerEvent {
  final int projectId;
  final int iid;
  final String objectId;

  TimeTrackerIssueSynced({this.projectId, this.iid, this.objectId});

  @override
  List<Object> get props => [projectId, iid, objectId];
}
