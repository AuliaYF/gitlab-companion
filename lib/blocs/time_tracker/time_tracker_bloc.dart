import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:duration/duration.dart';
import 'package:equatable/equatable.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/services/index.dart';

part 'time_tracker_event.dart';
part 'time_tracker_state.dart';

class TimeTrackerBloc extends Bloc<TimeTrackerEvent, TimeTrackerState> {
  final TimeTrackerService _service;
  Timer _timer;

  TimeTrackerBloc(this._service);

  @override
  TimeTrackerState get initialState => TimeTrackerInitial();

  @override
  Future<void> close() {
    _timer?.cancel();

    return super.close();
  }

  @override
  Stream<TimeTrackerState> mapEventToState(
    TimeTrackerEvent event,
  ) async* {
    if (event is TimeTrackerStarted) {
      yield TimeTrackerInProgress();

      var data = await _service.fetchLocalTimeStats();

      data.forEach((objectId, timeStats) {
        if (timeStats.running) {
          var dateDiff = DateTime.now().difference(timeStats.updatedAt);
          var totalTimeSpent =
              dateDiff.inSeconds.abs() + timeStats.totalTimeSpent;
          var humanTotalTimeSpent = prettyDuration(
            Duration(seconds: totalTimeSpent),
            abbreviated: true,
            delimiter: ' ',
            spacer: '',
          );

          data[objectId] = timeStats.copyWith(
            humanTotalTimeSpent: humanTotalTimeSpent,
            totalTimeSpent: totalTimeSpent,
            updatedAt: DateTime.now(),
          );
        }
      });

      yield TimeTrackerSuccess(timeStats: data);

      _timer = Timer.periodic(Duration(seconds: 1), (timer) async {
        var data = await _service.fetchLocalTimeStats();

        data.forEach((objectId, timeStats) {
          if (timeStats.running) {
            var totalTimeSpent = timeStats.totalTimeSpent + 1;
            var humanTotalTimeSpent = prettyDuration(
              Duration(seconds: totalTimeSpent),
              abbreviated: true,
              delimiter: ' ',
              spacer: '',
            );

            data[objectId] = timeStats.copyWith(
              humanTotalTimeSpent: humanTotalTimeSpent,
              totalTimeSpent: totalTimeSpent,
              updatedAt: DateTime.now(),
            );
          }
        });

        add(TimeTrackerUpdated(timeStats: data));
      });
    }

    if (event is TimeTrackerUpdated) {
      yield TimeTrackerInProgress();

      await _service.setLocalTimeStats(event.timeStats);

      yield TimeTrackerSuccess(timeStats: event.timeStats);
    }

    if (event is TimeTrackerObjectStarted) {
      yield TimeTrackerInProgress();

      var data = await _service.fetchLocalTimeStats();

      data[event.objectId] = event.initialTimeStats.copyWith(
        running: true,
        synced: false,
      );

      await _service.setLocalTimeStats(data);

      yield TimeTrackerSuccess(timeStats: data);
    }

    if (event is TimeTrackerObjectStopped) {
      yield TimeTrackerInProgress();

      var data = await _service.fetchLocalTimeStats();

      if (data.containsKey(event.objectId)) {
        data[event.objectId] = data[event.objectId].copyWith(running: false);
      }

      await _service.setLocalTimeStats(data);

      yield TimeTrackerSuccess(timeStats: data);
    }

    if (event is TimeTrackerObjectRemoved) {
      yield TimeTrackerInProgress();

      var data = await _service.fetchLocalTimeStats();

      data.remove(event.objectId);

      await _service.setLocalTimeStats(data);

      yield TimeTrackerSuccess(timeStats: data);
    }

    if (event is TimeTrackerIssueSynced) {
      yield TimeTrackerInProgress(
        syncingFromObjectId: event.objectId,
      );

      var data = await _service.fetchLocalTimeStats();

      if (data.containsKey(event.objectId)) {
        try {
          final response = await _service.setIssueTimeStats(
            event.projectId,
            event.iid,
            data[event.objectId].humanTotalTimeSpent,
          );

          data[event.objectId] = response.copyWith(synced: true);
        } catch (_) {}
      }

      await _service.setLocalTimeStats(data);

      yield TimeTrackerSuccess(
        timeStats: data,
        syncingFromObjectId: event.objectId,
      );
    }
  }
}
