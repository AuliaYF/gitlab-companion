part of 'time_tracker_bloc.dart';

abstract class TimeTrackerState extends Equatable {
  const TimeTrackerState();
}

class TimeTrackerInitial extends TimeTrackerState {
  @override
  List<Object> get props => [];
}

class TimeTrackerInProgress extends TimeTrackerState {
  final String syncingFromObjectId;

  TimeTrackerInProgress({this.syncingFromObjectId});
  @override
  List<Object> get props => [syncingFromObjectId];
}

class TimeTrackerSuccess extends TimeTrackerState {
  final Map<String, models.TimeStats> timeStats;
  final String syncingFromObjectId;

  TimeTrackerSuccess({this.timeStats, this.syncingFromObjectId});

  @override
  List<Object> get props => [timeStats, syncingFromObjectId];
}
