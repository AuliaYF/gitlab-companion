import 'package:dio/dio.dart';
import 'package:gitlab_companion/helpers/index.dart';
import 'package:gitlab_companion/models/index.dart' as models;

class TimeTrackerService {
  final Dio _dio;

  TimeTrackerService(this._dio);

  Future<Map<String, models.TimeStats>> fetchLocalTimeStats() {
    return PreferenceHelper.getTimeStats() ?? {};
  }

  Future<void> setLocalTimeStats(Map<String, models.TimeStats> timeStats) {
    return PreferenceHelper.setTimeStats(timeStats);
  }

  Future<models.TimeStats> setIssueTimeStats(
    int projectId,
    int iid,
    String humanTotalTimeSpent,
  ) async {
    var response = await _dio.post(
      '/projects/${projectId}/issues/${iid}/reset_spent_time',
    );

    if (response.statusCode != 200) throw Exception(response.statusMessage);

    response = await _dio.post(
      '/projects/${projectId}/issues/${iid}/add_spent_time',
      data: {
        'duration': humanTotalTimeSpent,
      },
    );

    if (response.statusCode != 201) throw Exception(response.statusMessage);

    return models.TimeStats.fromJson(response.data);
  }
}
