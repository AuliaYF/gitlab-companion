import 'package:dio/dio.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/responses/index.dart';

class ProjectService {
  final Dio _dio;

  ProjectService(this._dio);

  Future<FetchListResponse<models.Project>> fetchProjects(
      {int page = 1}) async {
    final response = await _dio.get('/projects', queryParameters: {
      'membership': true,
      'page': page,
    });

    if (response.statusCode != 200) throw Exception(response.statusMessage);

    var nextPage = num.tryParse(response.headers.value('X-Next-Page'));
    var hasMore = page < num.tryParse(response.headers.value('X-Total-Pages'));
    var projects = (response.data as List)
            ?.map((e) => e == null
                ? null
                : models.Project.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [];

    return FetchListResponse<models.Project>(
      nextPage: nextPage,
      hasMore: hasMore,
      data: projects,
    );
  }

  Future<FetchListResponse<models.Project>> fetchPersonalProjects(
    int userId, {
    int page = 1,
  }) async {
    final response = await _dio.get(
      '/users/${userId}/projects',
      queryParameters: {
        'page': page,
      },
    );

    if (response.statusCode != 200) throw Exception(response.statusMessage);

    var nextPage = num.tryParse(response.headers.value('X-Next-Page'));
    var hasMore = page < num.tryParse(response.headers.value('X-Total-Pages'));
    var projects = (response.data as List)
            ?.map((e) => e == null
                ? null
                : models.Project.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [];

    return FetchListResponse<models.Project>(
      nextPage: nextPage,
      hasMore: hasMore,
      data: projects,
    );
  }
}
