import 'package:dio/dio.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/responses/index.dart';

class IssueService {
  final Dio _dio;

  IssueService(this._dio);

  Future<FetchListResponse<models.Issue>> fetchIssues({
    int page = 1,
    models.IssueState state,
  }) async {
    final response = await _dio.get('/issues', queryParameters: {
      'with_labels_details': true,
      'page': page,
      'state': state != null ? models.IssueStateValues[state] : null,
    });

    if (response.statusCode != 200) throw Exception(response.statusMessage);

    var nextPage = num.tryParse(response.headers.value('X-Next-Page'));
    var hasMore = page < num.tryParse(response.headers.value('X-Total-Pages'));
    var issues = (response.data as List)
            ?.map((e) => e == null
                ? null
                : models.Issue.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [];

    return FetchListResponse<models.Issue>(
      nextPage: nextPage,
      hasMore: hasMore,
      data: issues,
    );
  }

  Future<models.Issue> fetchIssue(int projectId, int iid) async {
    final response = await _dio.get('/projects/${projectId}/issues/${iid}');

    if (response.statusCode != 200) throw Exception(response.statusMessage);

    (response.data as Map<String, dynamic>).remove('labels');

    return models.Issue.fromJson(response.data);
  }
}
