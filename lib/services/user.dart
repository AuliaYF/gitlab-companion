import 'package:dio/dio.dart';
import 'package:gitlab_companion/models/index.dart' as models;

class UserService {
  final Dio _dio;

  UserService(this._dio);

  Future<models.User> fetchCurrentUser() async {
    final response = await _dio.get('/user');

    if (response.statusCode != 200) throw Exception(response.statusMessage);

    return response.data == null
        ? null
        : models.User.fromJson(response.data as Map<String, dynamic>);
  }
}
