import 'package:dio/dio.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/responses/index.dart';

class NoteService {
  final Dio _dio;

  NoteService(this._dio);

  Future<FetchListResponse<models.Note>> fetchIssueNotes(
    int projectId,
    int iid, {
    int page = 1,
  }) async {
    final response = await _dio.get(
      '/projects/${projectId}/issues/${iid}/notes',
      queryParameters: {
        'sort': 'asc',
      },
    );

    if (response.statusCode != 200) throw Exception(response.statusMessage);

    var nextPage = num.tryParse(response.headers.value('X-Next-Page'));
    var hasMore = page < num.tryParse(response.headers.value('X-Total-Pages'));
    var notes = (response.data as List)
            ?.map((e) => e == null
                ? null
                : models.Note.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [];

    return FetchListResponse<models.Note>(
      nextPage: nextPage,
      hasMore: hasMore,
      data: notes,
    );
  }
}
