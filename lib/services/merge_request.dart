import 'package:dio/dio.dart';
import 'package:gitlab_companion/models/index.dart' as models;
import 'package:gitlab_companion/responses/index.dart';

class MergeRequestService {
  final Dio _dio;

  MergeRequestService(this._dio);

  Future<FetchListResponse<models.MergeRequest>> fetchMRs({
    int page = 1,
    models.MergeRequestState state,
  }) async {
    final response = await _dio.get(
      '/merge_requests',
      queryParameters: {
        'page': page,
        'with_labels_details': true,
        'scope': 'assigned_to_me',
        'state': state != null ? models.MergeRequestStateValues[state] : null,
      },
    );

    if (response.statusCode != 200) throw Exception(response.statusMessage);

    var nextPage = num.tryParse(response.headers.value('X-Next-Page'));
    var hasMore = page < num.tryParse(response.headers.value('X-Total-Pages'));
    var mergeRequests = (response.data as List)
            ?.map((e) => e == null
                ? null
                : models.MergeRequest.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [];

    return FetchListResponse<models.MergeRequest>(
      nextPage: nextPage,
      hasMore: hasMore,
      data: mergeRequests,
    );
  }
}
