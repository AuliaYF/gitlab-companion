import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gitlab_companion/blocs/issue_container/issue_container_bloc.dart';
import 'package:gitlab_companion/blocs/time_tracker/time_tracker_bloc.dart';
import 'package:gitlab_companion/helpers/index.dart';
import 'package:gitlab_companion/routes.dart';
import 'package:gitlab_companion/services/index.dart';
import 'package:gitlab_companion/session.dart';
import 'package:provider/provider.dart';

void main() {
  final dio = Dio();
  dio.options.baseUrl = 'https://gitlab.com/api/v4';
  dio.interceptors.add(
    InterceptorsWrapper(
      onRequest: (options) async {
        var hasToken = await PreferenceHelper.hasPersonalToken();

        if (hasToken) {
          var token = await PreferenceHelper.getPersonalToken();

          options.headers['Authorization'] = 'Bearer ${token}';
        }

        return options;
      },
    ),
  );

  runApp(MyApp(dio: dio));
}

class MyApp extends StatelessWidget {
  final Dio dio;

  const MyApp({Key key, @required this.dio})
      : assert(dio != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<Dio>.value(value: dio),
        ChangeNotifierProvider<Session>(
          create: (context) => Session(),
        )
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<IssueContainerBloc>(
            create: (context) => IssueContainerBloc(IssueService(dio)),
          ),
          BlocProvider<TimeTrackerBloc>(
            create: (context) => TimeTrackerBloc(TimeTrackerService(dio)),
          )
        ],
        child: MaterialApp(
          title: 'GitLab Companion',
          theme: ThemeData(
            primarySwatch: Colors.orange,
            brightness: Brightness.dark,
          ),
          routes: routes,
        ),
      ),
    );
  }
}
