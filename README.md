# GitLab Companion

[![Codemagic build status](https://api.codemagic.io/apps/5ede65533091aaa98b4eb7d7/5ede65533091aaa98b4eb7d6/status_badge.svg)](https://codemagic.io/apps/5ede65533091aaa98b4eb7d7/5ede65533091aaa98b4eb7d6/latest_build)

Another GitLab Companion app. Built on top of GitLab's API v4.

## Features

- [x] Projects (All/Personal)
- [x] Issues (Open/Closed/All)
- [x] Merge Requests (Open/Merged/Closed/All)
- [x] Show issue activities
- [ ] Post comment to an issue
- [x] Issue Time Tracking
- [ ] Merge Request Time Tracking

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
