# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.3](https://gitlab.com/AuliaYF/gitlab-companion/compare/v0.2.2...v0.2.3) (2020-06-09)


### Bug Fixes

* add missing android.permission.INTERNET ([855486d](https://gitlab.com/AuliaYF/gitlab-companion/commit/855486d4e7fd36d442cd189c03336536e49db017))

### [0.2.2](https://gitlab.com/AuliaYF/gitlab-companion/compare/v0.2.1...v0.2.2) (2020-06-09)


### Features

* **issue-detail:** add IssueContainerBloc to wrap all of the issues ([8df73cd](https://gitlab.com/AuliaYF/gitlab-companion/commit/8df73cd0c76e8bb7611714ad091b849f3b596897))


### Bug Fixes

* **time-tracking:** remove missingSeconds ([db11ebc](https://gitlab.com/AuliaYF/gitlab-companion/commit/db11ebcfdf64b812dcb51da7ac981db8e9c57449))

### [0.2.1](https://gitlab.com/AuliaYF/gitlab-companion/compare/v0.2.0...v0.2.1) (2020-06-08)


### Features

* **issue-detail:** add issue detail page ([e1568cf](https://gitlab.com/AuliaYF/gitlab-companion/commit/e1568cfd10194433eb6ebb527076f517522f1008))
* **issue-detail:** add time tracking ([eaecf03](https://gitlab.com/AuliaYF/gitlab-companion/commit/eaecf03d5b3e77cb810b49f54c22c0627312b1ae))


### Bug Fixes

* remove green border ([f8c7fe2](https://gitlab.com/AuliaYF/gitlab-companion/commit/f8c7fe2ff7d89a996fb5eda01bf8215916387d78))

## 0.2.0 (2020-06-07)


### Features

* **merge-request-list:** add merge request list page ([d653e09](https://gitlab.com/AuliaYF/gitlab-companion/commit/d653e09e1218346b9427944733ef68a7a5c0d083))
